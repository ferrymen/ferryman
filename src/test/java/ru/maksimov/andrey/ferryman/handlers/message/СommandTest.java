package ru.maksimov.andrey.ferryman.handlers.message;

import static org.junit.Assert.assertTrue;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

import ru.maksimov.andrey.golos4j.exception.SystemException;

public class СommandTest {

	@Test
	public void addChannl() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_ADD_CHANNL;
		boolean isMmatcher = matcher(command.getRegex(), "add channel [название канала]");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void addChannlFail() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_ADD_CHANNL;
		boolean isMmatcher = matcher(command.getRegex(), "add channe dsfd ");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void deleteChannl() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_DELETE_ACCOUNT;
		boolean isMmatcher = matcher(command.getRegex(), "delete account");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void deleteChannlFail() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_DELETE_ACCOUNT;
		boolean isMmatcher = matcher(command.getRegex(), "delete account [название канала]");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void listChannels() throws SystemException {
		Command command = Command.MAINMENU_ACCOUNT_LIST;
		boolean isMmatcher = matcher(command.getRegex(), "account list");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void listChannelsFail() throws SystemException {
		Command command = Command.MAINMENU_ACCOUNT_LIST;
		boolean isMmatcher = matcher(command.getRegex(), "account list ");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setLogin() throws SystemException {
		Command command = Command.MAINMENU_ACCOUNT_SETTINGS;
		boolean isMmatcher = matcher(command.getRegex(), "account settings fsdfs34FSDs fdsf ");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setLoginFail() throws SystemException {
		Command command = Command.MAINMENU_ACCOUNT_SETTINGS;
		boolean isMmatcher = matcher(command.getRegex(), "account setting fsdfs34FSDs fdsf ");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setKey() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_KEY;
		boolean isMmatcher = matcher(command.getRegex(), "set key 4dfgd");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setKeyFail() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_KEY;
		boolean isMmatcher = matcher(command.getRegex(), "set ke fdgdfg");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setMinCountPosts() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_MIN_COUNT_POSTS;
		boolean isMmatcher = matcher(command.getRegex(), "set min count 56");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setMinCountPostsFail() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_MIN_COUNT_POSTS;
		boolean isMmatcher = matcher(command.getRegex(), "set min count -56");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setMaxCountPosts() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_MAX_COUNT_POSTS;
		boolean isMmatcher = matcher(command.getRegex(), "set max count 56");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setMaxCountPostsFail() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_MAX_COUNT_POSTS;
		boolean isMmatcher = matcher(command.getRegex(), "set min count -56");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setMessageId() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_MESSAGE_ID;
		boolean isMmatcher = matcher(command.getRegex(), "set message id 342");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setMessageIdNegative() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_MESSAGE_ID;
		boolean isMmatcher = matcher(command.getRegex(), "set message id -342");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setMessageIdFail() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_MESSAGE_ID;
		boolean isMmatcher = matcher(command.getRegex(), "set message id dfgd45");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}


	@Test
	public void setAutosendTrue() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_AUTOSEND_MESSAGE;
		boolean isMmatcher = matcher(command.getRegex(), "set autosend message true");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setAutosendFalse() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_AUTOSEND_MESSAGE;
		boolean isMmatcher = matcher(command.getRegex(), "set autosend message false");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void setAutosendFail() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_SET_AUTOSEND_MESSAGE;
		boolean isMmatcher = matcher(command.getRegex(), "set autosend message fa");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}


	@Test
	public void sendMessage() throws SystemException {
		Command command = Command.MAINMENU_SEND_MESSAGE;
		boolean isMmatcher = matcher(command.getRegex(), "send message [название канала]");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void sendMessageFail() throws SystemException {
		Command command = Command.MAINMENU_SEND_MESSAGE;
		boolean isMmatcher = matcher(command.getRegex(), "set history messag dfgd45");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void mainmenuHelp() throws SystemException {
		Command command = Command.MAINMENU_HELP;
		boolean isMmatcher = matcher(command.getRegex(), "help");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void mainmenuHelpFail() throws SystemException {
		Command command = Command.MAINMENU_HELP;
		boolean isMmatcher = matcher(command.getRegex(), "help ");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void accountSettingsHelp() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_HELP;
		boolean isMmatcher = matcher(command.getRegex(), "help");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void accountSettingsHelpFail() throws SystemException {
		Command command = Command.ACCOUNT_SETTINGS_HELP;
		boolean isMmatcher = matcher(command.getRegex(), "help ");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void mainmenu() throws SystemException {
		Command command = Command.MAINMENU;
		boolean isMmatcher = matcher(command.getRegex(), "mainmenu");
		assertTrue(command.getErrorMessage(), isMmatcher);
	}

	@Test
	public void mainmenuFail() throws SystemException {
		Command command = Command.MAINMENU;
		boolean isMmatcher = matcher(command.getRegex(), "mainmenu ");
		assertFalse(command.getErrorMessage(), isMmatcher);
	}


	private boolean matcher(String regex, String input) {
		Pattern p = Pattern.compile(regex);
		Matcher matcher = p.matcher(input);
		return matcher.matches();
	}

}
