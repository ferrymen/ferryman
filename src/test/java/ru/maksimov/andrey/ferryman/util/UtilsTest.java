package ru.maksimov.andrey.ferryman.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.collect.Maps;

import ru.maksimov.andrey.ferryman.handlers.message.Menu;
import ru.maksimov.andrey.ferryman.structure.Account;
import ru.maksimov.andrey.ferryman.structure.UserMenu;
import ru.maksimov.andrey.golos4j.exception.SystemException;

public class UtilsTest {

	@Test
	public void convertString2ObjectUserMenu()
			throws SystemException, JsonParseException, JsonMappingException, IOException {
		String menu = "{\"step\":\"ACCOUNT\",\"value\":\"ferryman\"}";
		TypeReference<UserMenu> mapType = new TypeReference<UserMenu>() {
		};
		UserMenu menuDto = Utils.<UserMenu> convertString2Object(menu, mapType);
		assertTrue(menuDto != null);
		assertEquals(menuDto.getStep(), Menu.ACCOUNT);
		assertEquals(menuDto.getValue(), "ferryman");

	}

	@Test
	public void convertString2ObjectAccountSkipAutomaticTranslations()
			throws SystemException, JsonParseException, JsonMappingException, IOException {
		String strLogin2Account = "{\"login 2\":{\"login\":\"login 2\",\"privateKey\":\"private key 2\",\"channelId\":2,\"messageId\":2,\"maxCountPosts\":2,\"minCountPosts\":2}}";
		TypeReference<Map<String, Account>> mapType = new TypeReference<Map<String, Account>>() {
		};
		Map<String, Account> login2Account = Utils.convertString2Map(strLogin2Account, mapType);
		assertTrue(login2Account != null);
		assertTrue(login2Account.keySet() != null);
		Integer number = new Integer(2);
		Account account = login2Account.get("login " + number);
		assertEquals(account.getChannelId(), number);
		assertEquals(account.getLogin(), "login " + number);
		assertEquals(account.getMaxCountPosts(), number);
		assertEquals(account.getMessageId(), number);
		assertEquals(account.getMinCountPosts(), number);
		assertEquals(account.getPrivateKey(), "private key " + number);
		assertEquals(account.isAutomaticTranslations(), false);
	}

	@Test
	public void convertObjectAccount2StringAndBack()
			throws SystemException, JsonParseException, JsonMappingException, IOException {
		Map<String, Account> expectedLogin2Account = new HashMap<String, Account>();
		for (int number = 0; number < 3; number++) {
			Account account = new Account();
			if (number % 2 == 0) {
				account.setAutomaticTranslations(true);
			}
			account.setChannelId(number);
			account.setLogin("login " + number);
			account.setMaxCountPosts(number);
			account.setMessageId(number);
			account.setMinCountPosts(number);
			account.setPrivateKey("private key " + number);
			expectedLogin2Account.put(account.getLogin(), account);
		}
		String strLogin2Account = Utils.<String, Account> convertMap2String(expectedLogin2Account);
		TypeReference<Map<String, Account>> mapType = new TypeReference<Map<String, Account>>() {
		};

		Map<String, Account> actualLogin2Account = Utils.convertString2Map(strLogin2Account, mapType);
		assertTrue(Maps.difference(expectedLogin2Account, actualLogin2Account).areEqual());

	}

}
