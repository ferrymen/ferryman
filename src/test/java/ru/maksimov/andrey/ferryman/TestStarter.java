package ru.maksimov.andrey.ferryman;

/**
 * Тестовый стартер проекта
 * 
 * @author amaksimov
 */
public class TestStarter {

	public static void main(final String args[]) throws Throwable {
		StarterApplication.main(args);
	}
}
