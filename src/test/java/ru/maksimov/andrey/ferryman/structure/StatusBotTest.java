package ru.maksimov.andrey.ferryman.structure;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.golos4j.exception.SystemException;


public class StatusBotTest {

	@Test
	public void testOutTimeSecond() throws SystemException, InterruptedException, ParseException {
		long millis  = 1000;
		int woit = 1;
		StatusBot sBot= new StatusBot("test");
		Thread.sleep(woit * millis);
		DateFormat format = new SimpleDateFormat(DateUtils.ISO_TIME_FORMAT);
		Date date = format.parse(sBot.getOutTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		assertEquals(cal.get(Calendar.SECOND), woit);
	}

	@Test
	public void testOutTimeZero() throws SystemException, InterruptedException, ParseException {
		StatusBot sBot= new StatusBot("test");
		DateFormat format = new SimpleDateFormat(DateUtils.ISO_TIME_FORMAT);
		Date date = format.parse(sBot.getOutTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		assertEquals(cal.get(Calendar.SECOND), 0);
	}

	@Test
	public void testBeneficiary() throws SystemException, InterruptedException, ParseException {
		StatusBot sBot= new StatusBot("⛽ 59.70% \uD83D\uDD25 Можно публиковать\nАпвоты при установке бенефициара 10% через golos.cf/md");
		assertEquals(sBot.isBeneficiary(), true);
	}
}
