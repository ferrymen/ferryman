package ru.maksimov.andrey.ferryman.structure;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;

import ru.maksimov.andrey.commons.utils.DateUtils;

/**
 * Структура статус бота (vik)
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class StatusBot {

	private String message;
	private Date updatedAt;

	public StatusBot(String message) {
		this.message = message;
		this.updatedAt = new Date();

	}

	public String getMessage() {
		return message;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "UTC")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getOutTime() {
		Calendar updatedAtCal = Calendar.getInstance();
		updatedAtCal.setTime(updatedAt);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.HOUR_OF_DAY, -updatedAtCal.get(Calendar.HOUR_OF_DAY));
		now.add(Calendar.MINUTE, -updatedAtCal.get(Calendar.MINUTE));
		now.add(Calendar.SECOND, -updatedAtCal.get(Calendar.SECOND));
		SimpleDateFormat dt = new SimpleDateFormat(DateUtils.ISO_TIME_FORMAT);
		return dt.format(now.getTime());
	}

	public boolean isBeneficiary() {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, -3);
		if (updatedAt != null && now.getTime().before(updatedAt) && StringUtils.isNoneBlank(message)) {
				return message.contains("Апвоты при установке бенефициара 10%");
		}
		return false;
	}

}
