package ru.maksimov.andrey.ferryman.structure;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Структура пара логин-пароль
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class LoginKey {

	private String login;
	private String key;

	public String getLogin() {
		return login;
	}

	public String getKey() {
		return key;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.getLogin() == null ? 0 : this.getLogin().hashCode());
		result = prime * result + (this.getKey() == null ? 0 : this.getKey().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoginKey other = (LoginKey) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
