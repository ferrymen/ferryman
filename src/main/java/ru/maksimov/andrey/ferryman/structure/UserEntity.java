package ru.maksimov.andrey.ferryman.structure;

import org.telegram.bot.structure.IUser;

/**
 * Структура пользователь
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class UserEntity implements IUser {

	private  int userId; /// < ID of the user (provided by Telegram server)
	private Long userHash; /// < Hash of the user (provide by Telegram server)

	private String golosLogin;
	private String golosPrivateKey;
	private Integer minSizeMessages;

	public UserEntity(int userId) {
		this.userId = userId;
	}

	public UserEntity(UserEntity copy) {
		this.userId = copy.getUserId();
		this.userHash = copy.getUserHash();
	}

	@Override
	public int getUserId() {
		return this.userId;
	}

	@Override
	public Long getUserHash() {
		return userHash;
	}

	public void setUserHash(Long userHash) {
		this.userHash = userHash;
	}

	public String getGolosLogin() {
		return golosLogin;
	}

	public String getGolosPrivateKey() {
		return golosPrivateKey;
	}

	public Integer getMinSizeMessages() {
		return minSizeMessages;
	}

	public void setGolosLogin(String golosLogin) {
		this.golosLogin = golosLogin;
	}

	public void setGolosPrivateKey(String golosPrivateKey) {
		this.golosPrivateKey = golosPrivateKey;
	}

	public void setMinSizeMessages(Integer minSizeMessages) {
		this.minSizeMessages = minSizeMessages;
	}

	@Override
	public String toString() {
		return "" + this.userId;
	}
}
