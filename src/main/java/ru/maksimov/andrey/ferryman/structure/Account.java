package ru.maksimov.andrey.ferryman.structure;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Структура для храннения аккаунта пользователя
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class Account {

	private String login;
	private String privateKey;
	private Integer channelId;
	private Integer messageId;
	private Integer maxCountPosts;
	private Integer minCountPosts;
	private boolean isAutomaticTranslations;

	public String getLogin() {
		return login;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public Integer getMaxCountPosts() {
		return maxCountPosts;
	}

	public Integer getMinCountPosts() {
		return minCountPosts;
	}

	public boolean isAutomaticTranslations() {
		return isAutomaticTranslations;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public void setMaxCountPosts(Integer maxCountPosts) {
		this.maxCountPosts = maxCountPosts;
	}

	public void setMinCountPosts(Integer minCountPosts) {
		this.minCountPosts = minCountPosts;
	}

	public void setAutomaticTranslations(boolean isAutomaticTranslations) {
		this.isAutomaticTranslations = isAutomaticTranslations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.getLogin() == null ? 0 : this.getLogin().hashCode());
		result = prime * result + (this.getPrivateKey() == null ? 0 : this.getPrivateKey().hashCode());
		result = prime * result + (this.getChannelId() == null ? 0 : this.getChannelId().hashCode());
		result = prime * result + (this.getMessageId() == null ? 0 : this.getMessageId().hashCode());
		result = prime * result + (this.getMaxCountPosts() == null ? 0 : this.getMaxCountPosts().hashCode());
		result = prime * result + (this.getMinCountPosts() == null ? 0 : this.getMinCountPosts().hashCode());
		result = prime * result + Boolean.hashCode(this.isAutomaticTranslations());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (privateKey == null) {
			if (other.privateKey != null)
				return false;
		} else if (!privateKey.equals(other.privateKey)) {
			return false;
		}
		if (channelId == null) {
			if (other.channelId != null)
				return false;
		} else if (!channelId.equals(other.channelId)) {
			return false;
		}
		if (messageId == null) {
			if (other.messageId != null)
				return false;
		} else if (!messageId.equals(other.messageId)) {
			return false;
		}
		if (maxCountPosts == null) {
			if (other.maxCountPosts != null)
				return false;
		} else if (!maxCountPosts.equals(other.maxCountPosts)) {
			return false;
		}
		if (minCountPosts == null) {
			if (other.minCountPosts != null)
				return false;
		} else if (!minCountPosts.equals(other.minCountPosts)) {
			return false;
		}

		if (isAutomaticTranslations != other.isAutomaticTranslations) {
			return false;
		}
		return true;
	}

	/**
	 * Get state about account no password
	 *
	 * @return info account
	 */
	public String toMessage() {
		ReflectionToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
		return ReflectionToStringBuilder.toStringExclude(this, "privateKey");
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
