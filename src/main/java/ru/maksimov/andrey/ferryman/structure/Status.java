package ru.maksimov.andrey.ferryman.structure;

/**
 * Список состояний бота
 * 
 * @author amaksimov
 */
public enum Status {
	STOP, CHECK_WAITING, CHECK_ERROR, START, CLEAR
}
