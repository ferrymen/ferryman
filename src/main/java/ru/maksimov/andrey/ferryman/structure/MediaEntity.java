package ru.maksimov.andrey.ferryman.structure;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.telegram.api.file.location.TLFileLocation;

/**
 * Структура хранения медиа сообщения
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class MediaEntity {

	private String title;
	private String description;
	private String urlName;
	private String url;
	private TLFileLocation location;
	private Date date;
	private int size;

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getUrlName() {
		return urlName;
	}

	public String getUrl() {
		return url;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public TLFileLocation getLocation() {
		return location;
	}

	public void setLocation(TLFileLocation location) {
		this.location = location;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.getTitle() == null ? 0 : this.getTitle().hashCode());
		result = prime * result + (this.getDescription() == null ? 0 : this.getDescription().hashCode());
		result = prime * result + (this.getUrlName() == null ? 0 : this.getUrlName().hashCode());
		result = prime * result + (this.getUrl() == null ? 0 : this.getUrl().hashCode());
		result = prime * result + (this.getDate() == null ? 0 : this.getDate().hashCode());
		result = prime * result + this.getSize();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MediaEntity other = (MediaEntity) obj;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title)) {
			return false;
		}
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (urlName == null) {
			if (other.urlName != null)
				return false;
		} else if (!urlName.equals(other.urlName)) {
			return false;
		}
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url)) {
			return false;
		}
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date)) {
			return false;
		}
		if (size != other.size) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
