package ru.maksimov.andrey.ferryman.structure;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.telegram.bot.structure.Chat;

/**
 * Структура чат
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class ChatEntity implements Chat {
	private int id;
	private Long accessHash;
	private boolean isChannel;
	private String title;
	private String username;

	public ChatEntity(int id) {
		this.id = id;
	}

	public ChatEntity() {
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public Long getAccessHash() {
		return accessHash;
	}

	@Override
	public boolean isChannel() {
		return isChannel;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAccessHash(Long accessHash) {
		this.accessHash = accessHash;
	}

	public void setChannel(boolean channel) {
		isChannel = channel;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
