package ru.maksimov.andrey.ferryman.structure;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import ru.maksimov.andrey.ferryman.handlers.message.Menu;

/**
 * Меню пользователя
 * 
 * @author amaksimov
 */
public class UserMenu {

	private Menu step = Menu.MAINMENU;
	private String value;


	public Menu getStep() {
		return step;
	}

	public String getValue() {
		return value;
	}

	public void setStep(Menu step) {
		this.step = step;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.getStep() == null ? 0 : this.getStep().hashCode());
		result = prime * result + (this.getValue() == null ? 0 : this.getValue().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserMenu other = (UserMenu) obj;
		if (step == null) {
			if (other.step != null)
				return false;
		} else if (!step.equals(other.step)) {
			return false;
		}
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
