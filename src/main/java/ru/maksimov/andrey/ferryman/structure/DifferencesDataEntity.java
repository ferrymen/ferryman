package ru.maksimov.andrey.ferryman.structure;

/**
 * Структура разность дат
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class DifferencesDataEntity {
	private int botId;
	private int pts;
	private int date;
	private int seq;

	public int getBotId() {
		return botId;
	}

	public int getPts() {
		return pts;
	}

	public int getDate() {
		return date;
	}

	public int getSeq() {
		return seq;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public void setPts(int pts) {
		this.pts = pts;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

}
