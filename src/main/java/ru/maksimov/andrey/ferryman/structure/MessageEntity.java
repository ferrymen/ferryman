package ru.maksimov.andrey.ferryman.structure;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Структура сообщение
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class MessageEntity {
	private Integer id;
	private String text;
	private MediaEntity media;
	private boolean isDelete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public MediaEntity getMedia() {
		return media;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setMedia(MediaEntity media) {
		this.media = media;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.getText() == null ? 0 : this.getText().hashCode());
		result = prime * result + (this.getId() == null ? 0 : this.getId().hashCode());
		result = prime * result + (this.isDelete() ? 1 : 0);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageEntity other = (MessageEntity) obj;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text)) {
			return false;
		}
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}

		if (isDelete != other.isDelete) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
