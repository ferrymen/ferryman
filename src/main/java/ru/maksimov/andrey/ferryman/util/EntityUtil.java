package ru.maksimov.andrey.ferryman.util;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.telegram.api.file.location.TLAbsFileLocation;
import org.telegram.api.file.location.TLFileLocation;
import org.telegram.api.message.TLMessage;
import org.telegram.api.message.media.TLAbsMessageMedia;
import org.telegram.api.message.media.TLMessageMediaWebPage;
import org.telegram.api.photo.TLAbsPhoto;
import org.telegram.api.photo.TLPhoto;
import org.telegram.api.photo.size.TLAbsPhotoSize;
import org.telegram.api.photo.size.TLPhotoSize;
import org.telegram.api.webpage.TLAbsWebPage;
import org.telegram.api.webpage.TLWebPage;
import org.telegram.tl.TLVector;

import ru.maksimov.andrey.ferryman.database.entity.StatusBotEntity;
import ru.maksimov.andrey.ferryman.structure.MediaEntity;
import ru.maksimov.andrey.ferryman.structure.MessageEntity;
import ru.maksimov.andrey.ferryman.structure.StatusBot;

/**
 * Вспомогательный класс для работы с сущностью
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class EntityUtil {

	public static MessageEntity convertTLMessage2MessageEntity(TLMessage message) {
		MessageEntity messageEntity = new MessageEntity();
		messageEntity.setText(message.getMessage());
		messageEntity.setId(message.getId());
		//message.getFwdFrom()
		TLAbsMessageMedia media = message.getMedia();
		if (media instanceof TLMessageMediaWebPage) {
			TLAbsWebPage absWebPage = ((TLMessageMediaWebPage) media).getWebPage();
			if (absWebPage instanceof TLWebPage) {
				MediaEntity mediaEntity = convertTLWebPage2MediaEntity((TLWebPage) absWebPage);
				messageEntity.setMedia(mediaEntity);
			}
		}

		return messageEntity;
	}

	public static MediaEntity convertTLWebPage2MediaEntity(TLWebPage media) {
		MediaEntity mediaEntity = new MediaEntity();
		mediaEntity.setTitle(media.getTitle());
		mediaEntity.setDescription(media.getDescription());
		mediaEntity.setUrlName(media.getDisplay_url());
		mediaEntity.setUrl(media.getUrl());

		TLAbsPhoto tLAbsPhoto = media.getPhoto();
		if (tLAbsPhoto instanceof TLPhoto) {
			// Java timestamp is millisecods past epoch
			Instant instant = Instant.ofEpochSecond(((TLPhoto)tLAbsPhoto).getDate());
			ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
			Calendar cal =  GregorianCalendar.from(zdt);
			mediaEntity .setDate(cal.getTime());
			TLVector<TLAbsPhotoSize> sizes = ((TLPhoto) tLAbsPhoto).getSizes();
			if (sizes != null && !sizes.isEmpty()) {
				int maxSize = 0;
				TLFileLocation curentLocation = null;
				for (TLAbsPhotoSize tLAbsPhotoSize : sizes) {
					if (tLAbsPhotoSize instanceof TLPhotoSize) {
						if (((TLPhotoSize) tLAbsPhotoSize).getSize() > maxSize) {
							TLAbsFileLocation tLAbsFileLocation = ((TLPhotoSize) tLAbsPhotoSize).getLocation();
							if (tLAbsFileLocation instanceof TLFileLocation) {
								maxSize = ((TLPhotoSize) tLAbsPhotoSize).getSize();
								curentLocation = ((TLFileLocation) tLAbsFileLocation);
							}
						}
					}
				}
				mediaEntity.setLocation(curentLocation);
				mediaEntity.setSize(maxSize);
			}
		}
		return mediaEntity;
	}

	public static StatusBot convertStatusBotEntity2StatusBot(StatusBotEntity statusBotEntity) {
		if(statusBotEntity != null) {
			StatusBot statusBot = new StatusBot(statusBotEntity.getMessage());
			statusBot.setUpdatedAt(statusBotEntity.getUpdatedAt());
			return statusBot;
		}
		return null;
	}
}
