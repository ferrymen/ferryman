package ru.maksimov.andrey.ferryman.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;

import ru.maksimov.andrey.commons.exception.BusinessException;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.commons.utils.StringUtils;

/**
 * Вспомогательный класс для преобразования строк
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class Utils {

	private static final Logger LOG = LogManager.getLogger(Utils.class);

	public static byte[] convert(List<Byte> bytes) {
		byte[] binaryData = new byte[bytes.size()];
		int j = 0;
		for (Byte b : bytes) {
			binaryData[j++] = b.byteValue();
		}
		return binaryData;
	}

	/**
	 * Конвертация списка в строку
	 *
	 * @param values
	 *            список
	 * @return строка
	 */
	public static String convertList2String(List<Integer> values) {
		if (values != null && !values.isEmpty()) {
			try {
				String str = StringUtils.serialize(values);
				return str;
			} catch (VerificationException | BusinessException e) {
				LOG.warn("Unable set list: " + e.getMessage(), e);
			}
		}
		return new String();
	}

	/**
	 * Конвертация строки в спискок
	 *
	 * @param values
	 *            строка
	 * @return список
	 */
	public static List<Integer> convertString2List(String values) {
		if (values != null && !values.isEmpty()) {
			try {
				TypeReference<List<Integer>> listType = new TypeReference<List<Integer>>() {
				};
				List<Integer> list = StringUtils.deserialize(values, listType);
				return list;
			} catch (VerificationException | BusinessException e) {
				LOG.warn("Unable get list: " + e.getMessage(), e);
			}
		}
		return new ArrayList<Integer>();
	}

	/**
	 * Конвертация карты в строку
	 * 
	 * @param <K>
	 *            ключ
	 * @param <V>
	 *            значкние
	 *
	 * @param key2values
	 *            карта где ключ K , значение это V
	 * @return строка
	 */
	public static <K, V> String convertMap2String(Map<K, V> key2values) {
		if (key2values != null && !key2values.isEmpty()) {
			try {
				String str = StringUtils.serialize(key2values);
				return str;
			} catch (VerificationException | BusinessException e) {
				LOG.warn("Unable set map: " + e.getMessage(), e);
			}
		}
		return new String();
	}

	
	/**
	 * Конвертация строки в карту
	 * 
	 * @param <K>
	 *            ключ
	 * @param <V>
	 *            значение
	 *
	 * @param values
	 *            строка
	 * @return карта
	 */
	public static <K, V> Map<K, V> convertString2Map(String values, TypeReference<Map<K, V>> mapType) {
		if (values != null && !values.isEmpty()) {
			try {
				Map<K, V> map = StringUtils.deserialize(values, mapType);
				return map;
			} catch (VerificationException | BusinessException e) {
				LOG.warn("Unable get map: " + e.getMessage(), e);
			}
		}
		return new HashMap<K, V>();
	}

	/**
	 * Конвертация строки в UserMenu
	 * 
	 * тип объект
	 * @param <T>
	 * 
	 * @param values
	 *            строка
	 * @return объект
	 */
	public static <T> T convertString2Object(String values, TypeReference<T> mapType) {
		if (values != null && !values.isEmpty()) {
			try {
				T object = StringUtils.deserialize(values, mapType);
				return object;
			} catch (VerificationException | BusinessException e) {
				LOG.warn("Unable get Object: " + e.getMessage(), e);
			}
		}
		return null;
	}

	/**
	 * Конвертация объекта в строку
	 * 
	 * @param values
	 *            объект
	 * @return строка
	 */
	public static String convertObject2String(Object values) {
		if (values != null) {
			try {
				String str = StringUtils.serialize(values);
				return str;
			} catch (VerificationException | BusinessException e) {
				LOG.warn("Unable set Object: " + e.getMessage(), e);
			}
		}
		return new String();
	}
}
