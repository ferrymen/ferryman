package ru.maksimov.andrey.ferryman.service;


/**
 * Интерфейс по работе с файлами
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public interface FileService {

	/**
	 * Сохранить файл если его нету, если есть то не сохраняем
	 *
	 * @param authfile
	 *            путь до файла
	 * @param data
	 *            данные
	 */
	void saveData(String authfile, byte[] data);

	/**
	 * Загрузить файл если он есть
	 *
	 * @param authfile
	 *            данные
	 */
	byte[] loadData(String authfile);

	/**
	 * Удалить файл если он есть
	 *
	 * @param authfile
	 *            данные
	 */
	void deleteData(String authfile);
}
