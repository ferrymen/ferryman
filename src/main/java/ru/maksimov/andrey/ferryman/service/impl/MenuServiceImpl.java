package ru.maksimov.andrey.ferryman.service.impl;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.api.chat.TLAbsChat;
import org.telegram.api.chat.channel.TLChannel;
import org.telegram.api.contacts.TLResolvedPeer;
import org.telegram.api.functions.channels.TLRequestChannelsJoinChannel;
import org.telegram.api.functions.contacts.TLRequestContactsResolveUsername;
import org.telegram.api.functions.messages.TLRequestMessagesImportChatInvite;
import org.telegram.api.input.chat.TLInputChannel;
import org.telegram.api.updates.TLAbsUpdates;
import org.telegram.api.updates.TLUpdates;
import org.telegram.tl.TLVector;

import ru.maksimov.andrey.ferryman.config.Config;
import ru.maksimov.andrey.ferryman.database.DatabaseManagerImpl;
import ru.maksimov.andrey.ferryman.database.entity.UserGolosEntity;
import ru.maksimov.andrey.ferryman.handlers.message.Menu;
import ru.maksimov.andrey.ferryman.handlers.message.Text;
import ru.maksimov.andrey.ferryman.handlers.message.Command;
import ru.maksimov.andrey.ferryman.service.BotService;
import ru.maksimov.andrey.ferryman.service.MenuService;
import ru.maksimov.andrey.ferryman.structure.Account;
import ru.maksimov.andrey.ferryman.structure.UserMenu;
import ru.maksimov.andrey.golos4j.exception.BusinessException;

/**
 * Сервис по работе с меню
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
@Service
public class MenuServiceImpl implements MenuService {

	private static final Logger LOG = LogManager.getLogger(MenuServiceImpl.class);
	private static final String NL = "\n";
	private static final String SPACE = " ";
	

	private DatabaseManagerImpl databaseManager;
	private Config config;

	@Autowired
	private BotService botService;

	@Override
	public void setDatabaseManager(DatabaseManagerImpl databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public String executeCommand(String message, int userId) {
		if (config == null) {
			config = Config.getConfig();
		}
		UserGolosEntity userGolos = databaseManager.findByUserId(userId);
		if (userGolos == null) {
			userGolos = new UserGolosEntity();
			userGolos.setUserId(userId);
			return executeCommandMainmenu(message, userGolos);
		}
		LOG.info("Start get Menu");
		UserMenu menu = userGolos.getUserMenu();
		LOG.info(" Menu: " + menu);
		String replyMessage = null;
		switch (menu.getStep()) {
		case ACCOUNT:
			replyMessage = executeCommandAccount(message, userGolos, menu.getValue());
			break;
		default:
			replyMessage = executeCommandMainmenu(message, userGolos);
			break;
		}
		return replyMessage;

	}

	private String executeCommandAccount(String message, UserGolosEntity userGolos, String accountName) {
		LOG.info(" Start executeCommandAccount: " + message + " accountName" + accountName);
		String replyMessage = null;
		String value = null;
		Command curentCommand = Command.ACCOUNT_SETTINGS_HELP;
		if (StringUtils.isNotBlank(message)) {
			for (Command command : Command.values()) {
				Pattern p = Pattern.compile(command.getRegex());
				Matcher matcher = p.matcher(message);
				if (matcher.matches()) {
					if (matcher.groupCount() == 1) {
						value = matcher.group(1);
					}
					curentCommand = command;
					break;
				}
			}
		}
		LOG.info(" Start switch: ");
		switch (curentCommand) {
		case ACCOUNT_SETTINGS_ADD_CHANNL: {
			LOG.info(" Start ACCOUNT_SETTINGS_ADD_CHANNL: ");
			if (value != null) {
				try {
					List<String> channelNames = databaseManager.findAllChannelNameByUserId(userGolos.getUserId());
					if (channelNames.size() > config.getMaxSizeChannel()) {
						replyMessage = curentCommand.getErrorMessage() + Text.ERROR_MAX_SIZE_CHANNEL;
					} else {
						Integer channelId = joinChannel(value);
						if (channelId != null) {
							databaseManager.addChannelForUser(userGolos.getUserId(), accountName, channelId);
							replyMessage = curentCommand.getSuccessfulMessage() + value;
						}
					}
				} catch (BusinessException be) {
					replyMessage = curentCommand.getErrorMessage() + be.getMessage();
				}
			}
		}
			break;
		case ACCOUNT_SETTINGS_GET_STATE: {
			LOG.info(" Start ACCOUNT_SETTINGS_GET_STATE: ");
			try {
				Account account = databaseManager.findAccountByUserId(userGolos.getUserId(), accountName);
				String accountStr;
				if(account == null) {
					accountStr = "Нет данных";
				} else {
					accountStr = account.toMessage();
				}
				replyMessage = curentCommand.getSuccessfulMessage() + SPACE + accountStr;
			} catch (Exception e) {
				replyMessage = curentCommand.getErrorMessage() + e.getMessage();
			}
		}
			break;
		case ACCOUNT_SETTINGS_SET_MAX_COUNT_POSTS: {
			LOG.info(" Start ACCOUNT_SETTINGS_SET_MAX_COUNT_POSTS: ");
			if (value != null) {
				try {
					int number = Integer.parseInt(value);
					databaseManager.setMaxSizeMessagesForUser(userGolos.getUserId(), accountName, number);
					replyMessage = curentCommand.getSuccessfulMessage();
				} catch (NumberFormatException nfe) {
					replyMessage = curentCommand.getErrorMessage() + nfe.getMessage();
				} catch (BusinessException be) {
					replyMessage = curentCommand.getErrorMessage() + be.getMessage();
				}
			}
		}
			break;
		case ACCOUNT_SETTINGS_SET_MIN_COUNT_POSTS: {
			LOG.info(" Start ACCOUNT_SETTINGS_SET_MIN_COUNT_POSTS: ");
			if (value != null) {
				try {
					int number = Integer.parseInt(value);
					databaseManager.setMinSizeMessagesForUser(userGolos.getUserId(), accountName, number);
					replyMessage = curentCommand.getSuccessfulMessage();
				} catch (NumberFormatException nfe) {
					replyMessage = curentCommand.getErrorMessage() + nfe.getMessage();
				} catch (BusinessException be) {
					replyMessage = curentCommand.getErrorMessage() + be.getMessage();
				}
			}
		}
			break;
		case ACCOUNT_SETTINGS_SET_KEY: {
			LOG.info(" Start ACCOUNT_SETTINGS_SET_KEY: ");
			if (value != null) {
				try {
					if (StringUtils.isNoneBlank(value)) {
						databaseManager.setKeyForUser(userGolos.getUserId(), accountName, value);
						replyMessage = curentCommand.getSuccessfulMessage();
					}
				} catch (BusinessException be) {
					replyMessage = curentCommand.getErrorMessage() + be.getMessage();
				}
			}
		}
			break;
		case ACCOUNT_SETTINGS_SET_MESSAGE_ID: {
			LOG.info(" Start ACCOUNT_SETTINGS_SET_MESSAGE_ID: ");
			if (value != null) {
				try {
					int number = Integer.parseInt(value);
					databaseManager.setMessageIdForUser(userGolos.getUserId(), accountName, number);
					replyMessage = curentCommand.getSuccessfulMessage();
				} catch (Exception e) {
					replyMessage = curentCommand.getErrorMessage() + e.getMessage();
				}
			}
		}
			break;
		case ACCOUNT_SETTINGS_SET_AUTOSEND_MESSAGE: {
			LOG.info(" Start ACCOUNT_SETTINGS_SET_AUTOSEND: ");
			if (value != null) {
				try {
					boolean isAutosend = Boolean.parseBoolean(value);
					databaseManager.setAutosendMessageForUser(userGolos.getUserId(), accountName, isAutosend);
					replyMessage = curentCommand.getSuccessfulMessage();
				} catch (Exception e) {
					replyMessage = curentCommand.getErrorMessage() + e.getMessage();
				}
			}
		}
			break;
		case ACCOUNT_SETTINGS_DELETE_ACCOUNT: {
			LOG.info(" Start ACCOUNT_SETTINGS_DELETE_ACCOUNT: ");
			try {
				databaseManager.deleteAccountForUser(userGolos.getUserId(), accountName);
			} catch (BusinessException be) {
				replyMessage = curentCommand.getErrorMessage() + be.getMessage();
			}
			try {
				UserMenu userMenu = userGolos.getUserMenu();
				userMenu.setStep(Menu.MAINMENU);
				userMenu.setValue("");
				databaseManager.setUserMenu(userGolos.getUserId(), userMenu);
				replyMessage+= curentCommand.getSuccessfulMessage();
			} catch (Exception e) {
				replyMessage = curentCommand.getErrorMessage() + e.getMessage();
			}
		}
			break;
		case MAINMENU: {
			LOG.info(" Start MAINMENU: ");
			try {
				UserMenu userMenu = userGolos.getUserMenu();
				userMenu.setStep(Menu.MAINMENU);
				userMenu.setValue("");
				databaseManager.setUserMenu(userGolos.getUserId(), userMenu);
				replyMessage = curentCommand.getSuccessfulMessage() + NL + Text.MAINMENU_HELP;;
			} catch (Exception e) {
				replyMessage = curentCommand.getErrorMessage() + e.getMessage();
			}
		}
			break;
		case ACCOUNT_SETTINGS_HELP: {
			LOG.info(" Start ACCOUNT_SETTINGS_HELP: ");
			replyMessage = curentCommand.getSuccessfulMessage();
		}
			break;
		default:
			break;
		}
		if (replyMessage == null) {
			replyMessage = Command.ACCOUNT_SETTINGS_HELP.getSuccessfulMessage();
		}
		return replyMessage;
	}

	private String executeCommandMainmenu(String message, UserGolosEntity userGolos) {
		String replyMessage = null;
		String value = null;
		Command curentCommand = Command.MAINMENU_HELP;
		if (StringUtils.isNotBlank(message)) {
			for (Command command : Command.values()) {
				Pattern p = Pattern.compile(command.getRegex());
				Matcher matcher = p.matcher(message);
				if (matcher.matches()) {
					if (matcher.groupCount() == 1) {
						value = matcher.group(1);
					}
					curentCommand = command;
					break;
				}
			}
		}
		switch (curentCommand) {
		case MAINMENU_ACCOUNT_LIST: {
			try {
				Map<String, Account> login2Account = databaseManager.findAllAccountByUserId(userGolos.getUserId());
				StringBuilder sb = new StringBuilder();
				for (String account : login2Account.keySet()) {
					sb.append(account);
					sb.append("\n");
				}
				replyMessage = curentCommand.getSuccessfulMessage() + sb.toString();
			} catch (Exception e) {
				replyMessage = curentCommand.getErrorMessage() + e.getMessage();
			}
		}
			break;
		case MAINMENU_ACCOUNT_SETTINGS: {
			if (value != null) {
				try {
					if (StringUtils.isBlank(value)) {
						throw new BusinessException("не указан golos аккаунт");
					}
					Map<String, Account> login2Account = databaseManager.findAllAccountByUserId(userGolos.getUserId());
					Account account = login2Account.get(value);
					if (account == null) {
						if(login2Account.size() > config.getMaxSizeAccount()) {
							replyMessage = curentCommand.getErrorMessage() + Text.ERROR_MAX_SIZE_ACCOUNT;
							break;
						} else {
							databaseManager.setLoginForUser(userGolos.getUserId(), value);
						}
					}
					UserMenu userMenu = userGolos.getUserMenu();
					userMenu.setStep(Menu.ACCOUNT);
					userMenu.setValue(value);
					databaseManager.setUserMenu(userGolos.getUserId(), userMenu);
					replyMessage = curentCommand.getSuccessfulMessage() + value + NL + Text.ACCOUNT_HELP;
				} catch (Exception e) {
					replyMessage = curentCommand.getErrorMessage() + e.getMessage();
				}
			}
		}
			break;
		case MAINMENU_SEND_MESSAGE: {
			if (value != null) {
				try {
					botService.sendHistory(userGolos.getUserId(), value);
					replyMessage = curentCommand.getSuccessfulMessage();
				} catch (Exception e) {
					LOG.warn(curentCommand.getErrorMessage() + e.getMessage(), e);
					replyMessage = curentCommand.getErrorMessage() + e.getMessage();
				}
			}
		}
			break;
		case MAINMENU_HELP: {
			replyMessage = curentCommand.getSuccessfulMessage();
		}
			break;
		default:
			break;
		}
		if (replyMessage == null) {
			replyMessage = Command.MAINMENU_HELP.getSuccessfulMessage();
		}
		return replyMessage;
	}

	private Integer joinChannel(String hash) throws BusinessException {
		Integer id = null;
		LOG.info("Start add channel (joinChannel)" + hash);
		String msg = null;
		try {
			TLRequestContactsResolveUsername ru = new TLRequestContactsResolveUsername();
			ru.setUsername(hash);
			TLResolvedPeer peer = botService.doRpcCall(ru);
			TLRequestChannelsJoinChannel join = new TLRequestChannelsJoinChannel();
			TLVector<TLAbsChat> chats = peer.getChats();
			if (chats != null && !chats.isEmpty()) {
				TLAbsChat absChat = chats.get(0);
				if (absChat instanceof TLChannel) {
					TLInputChannel ch = new TLInputChannel();
					TLChannel channel = (TLChannel) absChat;
					ch.setChannelId(channel.getId());
					ch.setAccessHash(channel.getAccessHash());
					join.setChannel(ch);
					botService.doRpcCall(join);
					id = channel.getId();
				}
			}

		} catch (Exception e) {
			msg = e.getMessage();
		}
		if (msg != null) {
			TLRequestMessagesImportChatInvite in = new TLRequestMessagesImportChatInvite();
			in.setHash(hash);
			try {
				TLAbsUpdates absUpdates = botService.doRpcCall(in);
				if (absUpdates instanceof TLUpdates) {
					TLUpdates updates = (org.telegram.api.updates.TLUpdates) absUpdates;
					TLVector<TLAbsChat> chats = updates.getChats();
					id = getChannelId(chats);
				}
			} catch (Exception e) {
				throw new BusinessException("Unable add channel " + e.getMessage(), e);
			}
		}
		LOG.info("End add channel (joinChannel)" + hash + " id " + id);
		return id;
	}

	private Integer getChannelId(TLVector<TLAbsChat> chats) {
		Integer id = null;
		if (chats != null && !chats.isEmpty()) {
			TLAbsChat absChat = chats.get(0);
			if (absChat instanceof TLChannel) {
				id = absChat.getId();
			}
		}
		return id;
	}

	/*private int leaveChannel(String hash) throws BusinessException {
		Integer id = null;
		try {
			TLRequestContactsResolveUsername ru = new TLRequestContactsResolveUsername();
			ru.setUsername(hash);
			TLResolvedPeer peer = kernelComm.getApi().doRpcCall(ru);
			TLRequestChannelsLeaveChannel leave = new TLRequestChannelsLeaveChannel();
			TLVector<TLAbsChat> chats = peer.getChats();
			if (chats != null && !chats.isEmpty()) {
				TLAbsChat absChat = chats.get(0);
				if (absChat instanceof TLChannel) {
					TLInputChannel ch = new TLInputChannel();
					TLChannel channel = (TLChannel) absChat;
					ch.setChannelId(channel.getId());
					ch.setAccessHash(channel.getAccessHash());
					leave.setChannel(ch);
					kernelComm.getApi().doRpcCall(leave);
					id = channel.getId();
				}
			}

		} catch (Exception e) {
			throw new BusinessException("Unable leave channel " + e.getMessage(), e);
		}
		return id;
	}*/

}
