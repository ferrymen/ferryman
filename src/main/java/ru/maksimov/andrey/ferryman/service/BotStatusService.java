package ru.maksimov.andrey.ferryman.service;

/**
 * Интерфейс по работе со статусом бота (vik)
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public interface BotStatusService {

	/**
	 * оповестиить об Обновлении статусаса бота
	 * 
	 */
	void notifyStatusBot();

	/**
	 * Получить текущий id пользователя
	 * 
	 */
	int getCurrentUserId();
}
