package ru.maksimov.andrey.ferryman.service;

import java.io.IOException;

import org.telegram.api.engine.RpcException;
import org.telegram.bot.structure.IUser;
import org.telegram.tl.TLMethod;
import org.telegram.tl.TLObject;

import ru.maksimov.andrey.ferryman.structure.Status;
import ru.maksimov.andrey.ferryman.structure.StatusBot;
import ru.maksimov.andrey.golos4j.exception.BusinessException;

/**
 * Интерфейс по работе с ботом
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public interface BotService {

	/**
	 * Запустить бота
	 * 
	 */
	void doRun();

	/**
	 * Остановить бота
	 * 
	 */
	void doStop();

	/**
	 * Задать код проверки (авторизация)
	 * 
	 * @param code
	 *            код подтвержения
	 * 
	 * @return статус
	 * 
	 */
	Status setCodes(String code);

	/**
	 * Получить статус службы
	 * 
	 * @return статус
	 * 
	 */
	Status getStatus();

	/**
	 * Получить статус telegram api
	 * 
	 * @return статус api
	 * 
	 */
	int getStatusApi();

	/**
	 * Сброс кэша
	 * 
	 * @return резултат сброса
	 * 
	 */
	boolean cacheReset();

	/**
	 * Отпрвавить сообщение
	 * 
	 * @param message
	 *            сообщение
	 */
	void sendMessage(String message);

	/**
	 * Отпрвавить сообщения из истории
	 * 
	 * @param userId
	 *            идентификатор пользователя
	 * @param channelTitle
	 *            название канала
	 * 
	 */
	void sendHistory(int userId, String channelTitle) throws BusinessException;

	/**
	 * Отпрвавить сообщения из истории для всех пользователей у кого аккаунты
	 * настроены на авто отправку
	 * 
	 */
	void autoSendHistory() throws BusinessException;

	/**
	 * Получить Telegram API
	 * 
	 */

	/**
	 * Отметить как прочитанное
	 * 
	 * @param user
	 *            пользователь
	 * @param messageId
	 *            идентификатор сообщкения
	 */
	void performMarkAsRead(IUser user, int messageId) throws RpcException;

	/**
	 * Идентификатор пользователя
	 * 
	 */
	int getCurrentUserId();

	/**
	 * Отметить как прочитанное
	 * 
	 * @param user
	 *            пользователь
	 * @param message
	 *            сообщкение
	 * @param replayToMsg
	 *            идентификатор ответного сообщкения
	 */
	void sendMessageAsReply(IUser user, String message, Integer replayToMsg) throws RpcException;

	/**
	 * выполнить RPC вызов
	 * 
	 * @param method
	 *            метод
	 * @param T
	 *            тип возращаемого метода
	 * @return резултат выполения запроса
	 */
	<T extends TLObject> T doRpcCall(TLMethod<T> method) throws IOException, java.util.concurrent.TimeoutException;

	/**
	 * Получить счетчик до перезагрузки
	 * 
	 * @return счетчик
	 */
	int getCountReboot();

	/**
	 * Получить счетчик до получения статуса бота
	 * 
	 * @return счетчик
	 */
	int getCountGetStatusBot();

	/**
	 * Получить счетчик до автоотправки сообщений
	 * 
	 * @return счетчик
	 */
	int getCountAutoSendMessage();

	/**
	 * Задать счетчик до перезагрузки
	 * 
	 * @param countReboot
	 *            счетчик
	 */
	void setCountReboot(int countReboot);

	/**
	 * Задать счетчик до получения статуса бота
	 * 
	 * @param сountGetStatusBot
	 *            счетчик
	 */
	void setCountGetStatusBot(int сountGetStatusBot);

	/**
	 * Задать счетчик до автоотправки сообщений
	 * 
	 * @param countAutoSendMessage
	 *            счетчик
	 */
	void setCountAutoSendMessage(int countAutoSendMessage);

	/**
	 * Получить статус бота
	 * 
	 * @param isCache
	 *            признак получения из кэша
	 * @return статус бота
	 */
	StatusBot getStatusBot(boolean isCache) throws BusinessException;

	/**
	 * Удалить файлы авторизации
	 * 
	 * @return статус удаления
	 */
	Status deleteAuth();
}
