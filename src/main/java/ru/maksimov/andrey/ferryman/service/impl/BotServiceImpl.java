package ru.maksimov.andrey.ferryman.service.impl;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.telegram.bot.handlers.interfaces.IChatsHandler;
import org.telegram.bot.handlers.interfaces.IUsersHandler;
import org.telegram.bot.kernel.IKernelComm;
import org.telegram.bot.kernel.TelegramBot;
import org.telegram.bot.services.BotLogger;
import org.telegram.bot.structure.BotConfig;
import org.telegram.bot.structure.IUser;
import org.telegram.bot.structure.LoginStatus;
import org.telegram.tl.TLIntVector;
import org.telegram.tl.TLMethod;
import org.telegram.tl.TLObject;
import org.telegram.tl.TLVector;

import ru.maksimov.andrey.ferryman.bot.BotConfigImpl;
import ru.maksimov.andrey.ferryman.bot.ChatUpdatesBuilderImpl;
import ru.maksimov.andrey.ferryman.config.Config;
import ru.maksimov.andrey.ferryman.database.DatabaseManagerImpl;
import ru.maksimov.andrey.ferryman.database.entity.AuthorizationTelegramEntity;
import ru.maksimov.andrey.ferryman.database.entity.StatusBotEntity;
import ru.maksimov.andrey.ferryman.database.entity.UserGolosEntity;
import ru.maksimov.andrey.ferryman.database.repository.AuthorizationRepository;
import ru.maksimov.andrey.ferryman.database.repository.StatusBotRepository;
import ru.maksimov.andrey.ferryman.database.repository.UserRepository;
import ru.maksimov.andrey.ferryman.handlers.ChatsHandler;
import ru.maksimov.andrey.ferryman.handlers.CustomUpdatesHandler;
import ru.maksimov.andrey.ferryman.handlers.MessageHandler;
import ru.maksimov.andrey.ferryman.handlers.UsersHandler;
import ru.maksimov.andrey.ferryman.service.BotService;
import ru.maksimov.andrey.ferryman.service.BotStatusService;
import ru.maksimov.andrey.ferryman.service.FileService;
import ru.maksimov.andrey.ferryman.service.MenuService;
import ru.maksimov.andrey.ferryman.structure.Account;
import ru.maksimov.andrey.ferryman.structure.MessageEntity;
import ru.maksimov.andrey.ferryman.structure.Status;
import ru.maksimov.andrey.ferryman.structure.StatusBot;
import ru.maksimov.andrey.ferryman.structure.UserEntity;
import ru.maksimov.andrey.ferryman.task.SendHistiryMessageGolos;
import ru.maksimov.andrey.ferryman.task.SendMessageTask;
import ru.maksimov.andrey.ferryman.util.EntityUtil;
import ru.maksimov.andrey.golos4j.exception.BusinessException;

import org.telegram.api.chat.TLAbsChat;
import org.telegram.api.chat.channel.TLChannel;
import org.telegram.api.contacts.TLResolvedPeer;
import org.telegram.api.engine.RpcException;
import org.telegram.api.functions.contacts.TLRequestContactsResolveUsername;
import org.telegram.api.functions.messages.TLRequestMessagesGetAllChats;
import org.telegram.api.functions.messages.TLRequestMessagesGetHistory;
import org.telegram.api.functions.users.TLRequestUsersGetFullUser;
import org.telegram.api.input.peer.TLInputPeerChannel;
import org.telegram.api.input.user.TLInputUserSelf;
import org.telegram.api.message.TLAbsMessage;
import org.telegram.api.message.TLMessage;
import org.telegram.api.messages.TLAbsMessages;
import org.telegram.api.messages.chats.TLAbsMessagesChats;
import org.telegram.api.user.TLAbsUser;
import org.telegram.api.user.TLUser;

/**
 * Сервис по работе с ботом
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
@Service
public class BotServiceImpl implements BotService, BotStatusService {

	private static final Logger LOG = LogManager.getLogger(BotServiceImpl.class);

	private Status status = Status.STOP;
	private List<Boolean> isBusyApi = Collections.synchronizedList(new ArrayList<Boolean>());;
	private final Object monitorСode = new Object();
	private final Object getStatusBot = new Object();
	private String code = null;
	private DatabaseManagerImpl databaseManager;
	private TelegramBot kernel = null;
	private IChatsHandler chatsHandler = null;
	private ScheduledExecutorService scheduler = null;

	private int countReboot = 0;
	private int сountGetStatusBot = 0;
	private int countAutoSendMessage = 0;
	private Config config;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private StatusBotRepository statusBotRepository;

	@Autowired
	private AuthorizationRepository authorizationRepository;

	@Autowired
	private MenuService menuService;

	@Autowired
	MessageHandler messageHandler;

	@Autowired
	FileService fileService;

	@PostConstruct
	public void autorun() {
		AuthorizationTelegramEntity authorization = authorizationRepository
				.findById(getConfig().getTelegramPhonenumber()).orElse(null);
		if (authorization != null) {
			doRun();
		}
	}

	@Override
	public void doRun() {
		if (status == Status.STOP) {
			isBusyApi.clear();
			String authfile = getConfig().getTelegramAuthPath() + getConfig().getTelegramPhonenumber() + ".auth";
			byte[] data = fileService.loadData(authfile);
			// если файла авторизации нет загружаем из БД
			if (data == null) {
				AuthorizationTelegramEntity authorization = authorizationRepository
						.findById(getConfig().getTelegramPhonenumber()).orElse(null);
				if (authorization != null && authorization.getData() != null) {
					fileService.saveData(authfile, authorization.getData());
				}
			}
			databaseManager = DatabaseManagerImpl.getInstance();
			databaseManager.setUserRepository(userRepository);
			databaseManager.setStatusBotRepository(statusBotRepository);
			final BotConfig botConfig = new BotConfigImpl(getConfig().getTelegramPhonenumber(),
					getConfig().getTelegramAuthPath());
			final IUsersHandler usersHandler = new UsersHandler(databaseManager);
			chatsHandler = new ChatsHandler(databaseManager);
			menuService.setDatabaseManager(databaseManager);
			if (scheduler == null) {
				scheduler = Executors.newScheduledThreadPool(1);
				SendMessageTask sendMessageTask = new SendMessageTask();
				sendMessageTask.setBotService(this);
				scheduler.scheduleAtFixedRate(sendMessageTask, getConfig().getIntervalGetStatusBot(),
						getConfig().getIntervalGetStatusBot(), TimeUnit.MINUTES);
			}

			final ChatUpdatesBuilderImpl builder = new ChatUpdatesBuilderImpl(CustomUpdatesHandler.class);
			builder.setBotStatusService(this);
			builder.setDatabaseManager(databaseManager).setUsersHandler(usersHandler).setChatsHandler(chatsHandler)
					.setMessageHandler(messageHandler);

			try {
				BotLogger.setPathToLogs(getConfig().getTelegramLogPath());
				kernel = new TelegramBot(botConfig, builder, getConfig().getTelegramApikey(),
						getConfig().getTelegramApihash());
				LOG.info("Start kernel.init");
				LoginStatus status = kernel.init();
				LOG.info("kernel.init status:" + status.name());
				if (status == LoginStatus.CODESENT) {
					LOG.info("Enter a code:");
					this.status = Status.CHECK_WAITING;

					while (status != LoginStatus.ALREADYLOGGED) {
						synchronized (monitorСode) {
							if (code != null) {
								boolean success = kernel.getKernelAuth().setAuthCode(code.trim());
								if (success) {
									status = LoginStatus.ALREADYLOGGED;
									break;
								} else {
									LOG.warn("Error code  is incorrect");
									break;
								}
							} else {
								try {
									monitorСode.wait();
								} catch (InterruptedException e) {
									LOG.warn("Unable wait code " + e.getMessage(), e);
								}
							}
						}
					}

				}
				if (status == LoginStatus.ALREADYLOGGED) {
					kernel.startBot();
					this.status = Status.START;
					// если авторизовались успешно сохраняем данные в БД
					data = fileService.loadData(authfile);
					if (data != null) {
						AuthorizationTelegramEntity authorization = authorizationRepository
								.findById(getConfig().getTelegramPhonenumber()).orElse(new AuthorizationTelegramEntity(getConfig().getTelegramPhonenumber()));
						authorization.setData(data);
						authorizationRepository.save(authorization);
					}

				} else {
					this.status = Status.CHECK_ERROR;
					throw new Exception("Failed to log in: " + status);
				}
			} catch (Exception e) {
				LOG.error("Unable stasrt TelegramBot: " + e.getMessage(), e);
			}
		}
	}

	@Override
	public void doStop() {
		if (status != Status.STOP) {
			kernel.stopBot();
			code = null;
			status = Status.STOP;
		}
	}

	@Override
	public Status getStatus() {
		return this.status;
	}

	@Override
	public int getStatusApi() {
		return isBusyApi.size();
	}

	@Override
	public Status setCodes(String code) {
		this.code = code;
		synchronized (monitorСode) {
			monitorСode.notify();
		}
		return getStatus();
	}

	@Override
	public boolean cacheReset() {
		if (databaseManager == null) {
			return false;
		}
		return true;
	}

	@Override
	public void sendMessage(String message) {
		try {
			isBusyApi.add(true);
			IKernelComm kernelComm = kernel.getKernelComm();
			final IUser user = databaseManager.getUserById(kernelComm.getCurrentUserId());
			try {
				if (user != null) {
					kernelComm.sendMessage(user, message);
				}
			} catch (Exception e) {
				LOG.error("Unable sended message: " + e.getMessage(), e);
			}
		} finally {
			isBusyApi.remove(isBusyApi.size() - 1);
		}

	}

	@Override
	public void sendHistory(int userId, String channelTitle) throws BusinessException {
		boolean isBeneficiary = false;
		StatusBot statusBot = getStatusBot(true);
		if(statusBot != null) {
			isBeneficiary = statusBot.isBeneficiary();
		}
		sendHistory(userId, channelTitle, null, isBeneficiary);
	}

	@Override
	public void autoSendHistory() throws BusinessException {
		long countUsers = databaseManager.countUsers();
		boolean isBeneficiary = false;
		StatusBot statusBot = getStatusBot(true);
		if(statusBot != null) {
			isBeneficiary = statusBot.isBeneficiary();
		}
		for (int i = 0; i < countUsers; i++) {
			Set<UserGolosEntity> users = databaseManager.findAllUsers(PageRequest.of(i, 1));
			for (UserGolosEntity user : users) {
				Map<String, Account> login2Account = user.getMapLogin2Account();
				for (String login : login2Account.keySet()) {
					Account account = login2Account.get(login);
					if (account != null) {
						if (account.isAutomaticTranslations()) {
							try {
								sendHistory(user.getUserId(), null, account.getChannelId(), isBeneficiary);
							} catch (BusinessException be) {
								LOG.warn("Unable send historye, with param login " + login + " " + be.getMessage(), be);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public int getCurrentUserId() {
		try {
			isBusyApi.add(true);
			return kernel.getKernelComm().getCurrentUserId();
		} finally {
			isBusyApi.remove(isBusyApi.size() - 1);
		}
	}

	@Override
	public void sendMessageAsReply(IUser user, String message, Integer replayToMsg) throws RpcException {
		try {
			isBusyApi.add(true);
			kernel.getKernelComm().sendMessageAsReply(user, message, replayToMsg);
		} finally {
			isBusyApi.remove(isBusyApi.size() - 1);
		}
	}

	@Override
	public void performMarkAsRead(IUser user, int messageId) throws RpcException {
		try {
			isBusyApi.add(true);
			kernel.getKernelComm().performMarkAsRead(user, messageId);
		} finally {
			isBusyApi.remove(isBusyApi.size() - 1);
		}
	}

	@Override
	public <T extends TLObject> T doRpcCall(TLMethod<T> method)
			throws IOException, java.util.concurrent.TimeoutException {
		try {
			isBusyApi.add(true);
			return kernel.getKernelComm().getApi().doRpcCall(method);
		} finally {
			isBusyApi.remove(isBusyApi.size() - 1);
		}
	}

	@Override
	public int getCountReboot() {
		return countReboot;
	}

	@Override
	public int getCountGetStatusBot() {
		return сountGetStatusBot;
	}

	@Override
	public int getCountAutoSendMessage() {
		return countAutoSendMessage;
	}

	@Override
	public void setCountReboot(int countReboot) {
		this.countReboot = countReboot;
	}

	@Override
	public void setCountGetStatusBot(int сountGetStatusBot) {
		this.сountGetStatusBot = сountGetStatusBot;
	}

	@Override
	public void setCountAutoSendMessage(int countAutoSendMessage) {
		this.countAutoSendMessage = countAutoSendMessage;
	}

	@Override
	public StatusBot getStatusBot(boolean isCache) throws BusinessException {
		int botId = getConfig().getBotId();
		if (!isCache) {
			try {
				isBusyApi.add(true);
				IKernelComm kernelComm = kernel.getKernelComm();

				IUser user = databaseManager.getUserById(botId);
				if (user == null) {
					user = getUserById(botId, getConfig().getBotName());
				}
				try {
					if (user != null) {
						kernelComm.sendMessage(user, "/vik");
						// ожидаем ответ от бота
						synchronized (getStatusBot) {
							try {
								getStatusBot.wait(60000);
							} catch (InterruptedException e) {
								LOG.warn("Unable wait bot status " + e.getMessage(), e);
							} catch (IllegalArgumentException e) {
								LOG.warn("Unable wait bot status, timeout read status ");
							}
						}
					}
				} catch (Exception e) {
					LOG.error("Unable sended message: " + e.getMessage(), e);
				}
			} finally {
				isBusyApi.remove(isBusyApi.size() - 1);
			}
		}
		StatusBotEntity statusBotEntity = databaseManager.getStatusBot(botId);
		return EntityUtil.convertStatusBotEntity2StatusBot(statusBotEntity);
	}

	@Override
	public void notifyStatusBot() {
		synchronized (getStatusBot) {
			getStatusBot.notifyAll();
		}
	}


	@Override
	public Status deleteAuth() {
		String authfile = getConfig().getTelegramAuthPath() + getConfig().getTelegramPhonenumber() + ".auth";
		fileService.deleteData(authfile);
		authorizationRepository.deleteById(getConfig().getTelegramPhonenumber());
		return Status.CLEAR;
	}

	private void sendHistory(int userId, String channelTitle, Integer channelId, boolean isBeneficiary) throws BusinessException {
		if (channelId == null && StringUtils.isBlank(channelTitle)) {
			throw new BusinessException("Error no specified channel title or channel id");
		}
		try {
			isBusyApi.add(true);
			TLRequestMessagesGetAllChats getAllChats = new TLRequestMessagesGetAllChats();
			getAllChats.setExceptIds(new TLIntVector());
			try {
				TLAbsMessagesChats messagesChats = kernel.getKernelComm().getApi().doRpcCall(getAllChats);
				TLVector<TLAbsChat> absChats = messagesChats.getChats();
				for (TLAbsChat absChat : absChats) {
					if (absChat instanceof TLChannel) {

						if ((channelId != null && channelId.equals(absChat.getId()))
								|| (channelTitle != null && channelTitle.equals(((TLChannel) absChat).getTitle()))) {
							channelTitle = ((TLChannel) absChat).getTitle();
							Long channelHash = ((TLChannel) absChat).getAccessHash();
							TLRequestMessagesGetHistory getHistory = new TLRequestMessagesGetHistory();
							TLInputPeerChannel channel = new TLInputPeerChannel();
							channel.setChannelId(absChat.getId());
							channel.setAccessHash(channelHash);
							UserGolosEntity userGolos = databaseManager.findByUserId(userId);
							Integer historyMinId = 0;
							Account account = null;
							if (userGolos != null) {
								account = userGolos.getAccountByChannelId(absChat.getId());
								if (account != null) {
									historyMinId = account.getMessageId();
									if (account.getMaxCountPosts() != null
											&& account.getMaxCountPosts() < getConfig().getMaxSizeMessages()) {
										getHistory.setLimit(account.getMaxCountPosts());
									} else {
										getHistory.setLimit(getConfig().getMaxSizeMessages());
									}
								} else {
									throw new BusinessException(
											"Unable found account for channel title  " + channelTitle);
								}
								if (historyMinId != null && historyMinId > 0) {
									getHistory.setMinId(historyMinId);
								} else if (historyMinId != null) {
									getHistory.setOffsetId(historyMinId * -1);
								}
							}
							getHistory.setPeer(channel);
							try {
								TLAbsMessages tAbsMessages = kernel.getKernelComm().getApi().doRpcCall(getHistory);
								TLVector<TLAbsMessage> absMessages = tAbsMessages.getMessages();
								if (account.getMinCountPosts() != null
										&& account.getMinCountPosts() > absMessages.size()) {
									throw new BusinessException("unable send messages for channel " + channelTitle
											+ ". minimum number of messages " + account.getMinCountPosts() + " > "
											+ absMessages.size());
								}
								Set<MessageEntity> messages = new HashSet<MessageEntity>();
								for (TLAbsMessage absMessage : absMessages) {
									if (absMessage instanceof TLMessage) {
										TLMessage message = (TLMessage) absMessage;
										if (historyMinId < 0 && message.getId() < (historyMinId * -1)) {
											historyMinId = message.getId() * -1;
										} else if (message.getId() > historyMinId || historyMinId == 0) {
											historyMinId = message.getId();
										}
										MessageEntity messageEntity = EntityUtil
												.convertTLMessage2MessageEntity(message);
										messages.add(messageEntity);
									}
								}
								SendHistiryMessageGolos sendHistiry = new SendHistiryMessageGolos();
								sendHistiry.setBeneficiary(isBeneficiary);
								if (kernel != null) {
									sendHistiry.setDownloader(kernel.getKernelComm().getDownloader());
								}
								String username = ((TLChannel) absChat).getUsername();
								sendHistiry.sendMessage(messages, absChat.getId(), channelTitle, username, account);
								databaseManager.setMessageIdForUser(userId, account.getLogin(), historyMinId);
							} catch (IOException | java.util.concurrent.TimeoutException e) {
								LOG.error("Unable get history message for userId: " + userId + " " + e.getMessage(), e);
							}
						}

						if (chatsHandler != null) {
							chatsHandler.onChats(Collections.singletonList(absChat));
						}
					}
				}
			} catch (IOException | java.util.concurrent.TimeoutException e) {
				String message = "Unable get history message for userId: " + userId + " " + e.getMessage();
				LOG.error(message, e);
				throw new BusinessException(message);
			}
		} finally {
			isBusyApi.remove(isBusyApi.size() - 1);
		}
	}

	private IUser getUserById(int userId, String userName) {
		UserEntity user = null;
		try {
			TLRequestContactsResolveUsername contacts = new TLRequestContactsResolveUsername();
			contacts.setUsername(userName);
			TLRequestUsersGetFullUser getUser = new TLRequestUsersGetFullUser();
			getUser.setId(new TLInputUserSelf());
			IKernelComm kernelComm = kernel.getKernelComm();
			TLResolvedPeer respons = kernelComm.doRpcCallSync(contacts);
			if (respons instanceof TLResolvedPeer) {
				if (respons != null && respons.getUsers() != null) {
					TLVector<TLAbsUser> users = respons.getUsers();
					for (TLAbsUser u : users) {
						if (u.getId() == userId && u instanceof TLUser) {
							user = new UserEntity(userId);
							user.setUserHash(((TLUser) u).getAccessHash());
							break;
						}
					}
				}

			}
		} catch (Exception ex) {
			LOG.error("Exception getting user info userId " + userId, ex);
		}
		return user;
	}

	private Config getConfig() {
		if (config == null) {
			config = Config.getConfig();
		}
		return config;
	}
}
