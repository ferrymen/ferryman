package ru.maksimov.andrey.ferryman.service.impl;

import static org.telegram.tl.StreamingUtils.readBytes;
import static org.telegram.tl.StreamingUtils.readInt;
import static org.telegram.tl.StreamingUtils.readLong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.CRC32;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.ferryman.service.FileService;

/**
 * Сервис по работе с файлами
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
@Service
public class FileServiceImpl implements FileService {

	private static final Logger LOG = LogManager.getLogger(FileServiceImpl.class);

	@Override
	public void saveData(String authfile, byte[] data) {
		File file = new File(authfile);
		if (file.exists()) {
			return;
		}
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(file);
			writeInt(data.length, os);
			writeByteArray(data, os);
			CRC32 crc32 = new CRC32();
			crc32.update(data);
			writeLong(crc32.getValue(), os);
			os.flush();
			os.getFD().sync();
			os.close();
			os = null;
		} catch (IOException e) {
			LOG.error("Unable save file " + authfile + " " + e.getMessage(), e);
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					LOG.error("Unable save and close file " + authfile + " " + e.getMessage(), e);
				}
			}
		}
	}

	@Override
	public void deleteData(String authfile) {
		File file = new File(authfile);
		if (!file.exists()) {
			return;
		}
		file.delete();
	}

	@Override
	public byte[] loadData(String authfile) {
		File file = new File(authfile);
		if (!file.exists())
			return null;

		FileInputStream is = null;
		try {
			is = new FileInputStream(file);
			int len = readInt(is);
			byte[] res = readBytes(len, is);
			CRC32 crc32 = new CRC32();
			crc32.update(res);
			long crc = readLong(is);
			if (crc32.getValue() != crc) {
				return null;
			}
			return res;
		} catch (IOException e) {
			LOG.error("Unable load file " + authfile + " " + e.getMessage(), e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LOG.error("Unable load file and close " + authfile + " " + e.getMessage(), e);
				}
			}
		}
		return null;
	}

	/**
	 * Writing byte to stream
	 *
	 * @param v
	 *            value
	 * @param stream
	 *            destination stream
	 * @throws IOException
	 */
	private static void writeByte(byte v, OutputStream stream) throws IOException {
		stream.write(v);
	}

	/**
	 * Writing byte array to stream
	 *
	 * @param data
	 *            data
	 * @param stream
	 *            destination stream
	 * @throws IOException
	 */
	private static void writeByteArray(byte[] data, OutputStream stream) throws IOException {
		stream.write(data);
	}

	/**
	 * Writing int to stream
	 *
	 * @param v
	 *            value
	 * @param stream
	 *            destination stream
	 * @throws IOException
	 */
	private static void writeInt(int v, OutputStream stream) throws IOException {
		writeByte((byte) (v & 0xFF), stream);
		writeByte((byte) ((v >> 8) & 0xFF), stream);
		writeByte((byte) ((v >> 16) & 0xFF), stream);
		writeByte((byte) ((v >> 24) & 0xFF), stream);
	}

	/**
	 * Writing long to stream
	 *
	 * @param v
	 *            value
	 * @param stream
	 *            destination stream
	 * @throws IOException
	 */
	private static void writeLong(long v, OutputStream stream) throws IOException {
		writeByte((byte) (v & 0xFF), stream);
		writeByte((byte) ((v >> 8) & 0xFF), stream);
		writeByte((byte) ((v >> 16) & 0xFF), stream);
		writeByte((byte) ((v >> 24) & 0xFF), stream);

		writeByte((byte) ((v >> 32) & 0xFF), stream);
		writeByte((byte) ((v >> 40) & 0xFF), stream);
		writeByte((byte) ((v >> 48) & 0xFF), stream);
		writeByte((byte) ((v >> 56) & 0xFF), stream);
	}
}
