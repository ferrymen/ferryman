package ru.maksimov.andrey.ferryman.service;

import ru.maksimov.andrey.ferryman.database.DatabaseManagerImpl;

/**
 * Интерфейс по работе с меню
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public interface MenuService {

	/**
	 * Выполнить команду
	 * 
	 * @param criterion
	 *            критерии
	 * @return резултат выполения
	 */
	public String executeCommand(String message, int userId);

	/**
	 * Задать менеджер для работы с бд
	 * 
	 * @param databaseManager
	 *            менеджер для работы с бд
	 */
	void setDatabaseManager(DatabaseManagerImpl databaseManager);

}
