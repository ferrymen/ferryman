package ru.maksimov.andrey.ferryman.bot;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.telegram.bot.ChatUpdatesBuilder;
import org.telegram.bot.handlers.UpdatesHandlerBase;
import org.telegram.bot.handlers.interfaces.IChatsHandler;
import org.telegram.bot.handlers.interfaces.IUsersHandler;
import org.telegram.bot.kernel.IKernelComm;
import org.telegram.bot.kernel.database.DatabaseManager;
import org.telegram.bot.kernel.differenceparameters.IDifferenceParametersService;

import ru.maksimov.andrey.ferryman.database.DatabaseManagerImpl;
import ru.maksimov.andrey.ferryman.handlers.CustomUpdatesHandler;
import ru.maksimov.andrey.ferryman.handlers.MessageHandler;
import ru.maksimov.andrey.ferryman.service.BotStatusService;

/**
 * Класс поднимвающий слушателя чатов
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class ChatUpdatesBuilderImpl implements ChatUpdatesBuilder {

	private final Class<CustomUpdatesHandler> updatesHandlerBase;
	private IKernelComm kernelComm;
	private IUsersHandler usersHandler;
	private IChatsHandler chatsHandler;
	private MessageHandler messageHandler;
	private IDifferenceParametersService differenceParametersService;
	private DatabaseManagerImpl databaseManager;
	private BotStatusService botStatusService;

	public ChatUpdatesBuilderImpl(Class<CustomUpdatesHandler> updatesHandlerBase) {
		this.updatesHandlerBase = updatesHandlerBase;
	}

	@Override
	public void setKernelComm(IKernelComm kernelComm) {
		this.kernelComm = kernelComm;

	}

	@Override
	public void setDifferenceParametersService(IDifferenceParametersService differenceParametersService) {
		this.differenceParametersService = differenceParametersService;
	}

	@Override
	public DatabaseManager getDatabaseManager() {
		return databaseManager;
	}

	public ChatUpdatesBuilderImpl setUsersHandler(IUsersHandler usersHandler) {
		this.usersHandler = usersHandler;
		return this;
	}

	public ChatUpdatesBuilderImpl setChatsHandler(IChatsHandler chatsHandler) {
		this.chatsHandler = chatsHandler;
		return this;
	}

	public ChatUpdatesBuilderImpl setMessageHandler(MessageHandler messageHandler) {
		this.messageHandler = messageHandler;
		return this;
	}


	public ChatUpdatesBuilderImpl setDatabaseManager(DatabaseManagerImpl databaseManager) {
		this.databaseManager = databaseManager;
		return this;
	}

	@Override
	public UpdatesHandlerBase build()
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		if (kernelComm == null) {
			throw new NullPointerException("Can't build the handler without a KernelComm");
		}
		if (differenceParametersService == null) {
			throw new NullPointerException("Can't build the handler without a differenceParamtersService");
		}
		final Constructor<CustomUpdatesHandler> constructor = updatesHandlerBase.getConstructor(IKernelComm.class,
				IDifferenceParametersService.class, DatabaseManagerImpl.class);
		final CustomUpdatesHandler updatesHandler = constructor.newInstance(kernelComm, differenceParametersService,
				getDatabaseManager());
		updatesHandler.setHandlers(messageHandler, usersHandler, chatsHandler);
		updatesHandler.setBotStatusService(botStatusService);
		return updatesHandler;
	}

	public void setBotStatusService(BotStatusService botStatusService) {
		this.botStatusService = botStatusService;
	}

}
