package ru.maksimov.andrey.ferryman.bot;

import org.telegram.bot.structure.BotConfig;

/**
 * Конфигурация для бота
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class BotConfigImpl extends BotConfig {

	private static final long serialVersionUID = 1L;

	private String phoneNumber;

	public BotConfigImpl(String phoneNumber, String path) {
		this.phoneNumber = phoneNumber;
		setAuthfile(path + phoneNumber + ".auth");
	}

	@Override
	public String getPhoneNumber() {
		return phoneNumber;
	}

	@Override
	public String getBotToken() {
		return null;
	}

	@Override
	public boolean isBot() {
		return false;
	}
}
