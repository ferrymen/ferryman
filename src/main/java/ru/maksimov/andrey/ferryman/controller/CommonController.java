package ru.maksimov.andrey.ferryman.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ru.maksimov.andrey.ferryman.service.BotService;
import ru.maksimov.andrey.ferryman.structure.Status;
import ru.maksimov.andrey.ferryman.structure.StatusBot;
import ru.maksimov.andrey.golos4j.exception.BusinessException;

/**
 * Основной контроллер
 * 
 * @author amaksimov
 */
@RestController
@RequestMapping("/rest/")
public class CommonController {

	@Autowired
	private BotService botService;

	@RequestMapping(value = "start", method = RequestMethod.GET)
	public String doRun() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				botService.doRun();
			}
		}).start();

		return "doRun";
	}

	@RequestMapping(value = "stop", method = RequestMethod.GET)
	public String doStop() {
		botService.doStop();
		Status status = botService.getStatus();
		return status.name();
	}

	@RequestMapping(value = "get/status", method = RequestMethod.GET)
	public String getStatus() {
		Status status = botService.getStatus();
		return status.name();
	}

	@RequestMapping(value = "set/code/{code}", method = RequestMethod.GET)
	public String setCode(@PathVariable String code) throws BusinessException {
		if (StringUtils.isBlank(code)) {
			throw new BusinessException("Unable enter code. Code is empty");
		}
		Status status = botService.setCodes(code);
		return status.name();
	}

	@RequestMapping(value = "delete/auth", method = RequestMethod.GET)
	public String deleteAuth() throws BusinessException {
		return botService.deleteAuth().name();
	}

	@RequestMapping(value = "cache/reset", method = RequestMethod.GET)
	public boolean cacheReset() {
		boolean isReset = botService.cacheReset();
		return isReset;
	}

	@RequestMapping(value = "get/status/bot", method = RequestMethod.GET)
	public ResponseEntity<?> getStatusBot(@RequestParam("cache") boolean isCache) {
		StatusBot statusBot = null;
		try {
			statusBot = botService.getStatusBot(isCache);
		} catch (BusinessException e) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Unable get status bot. " + e.getMessage());
		}
		if (statusBot == null || StringUtils.isBlank(statusBot.getMessage())) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Status bot is empty");
		}
		return new ResponseEntity<>(statusBot, HttpStatus.OK);
	}
}
