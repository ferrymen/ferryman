package ru.maksimov.andrey.ferryman.config;

import java.io.File;
import java.net.URL;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Конфиг для приложения динамический
 * 
 * @author amaksimov
 */
public class Config {

	private static final Logger LOG = LogManager.getLogger(Config.class);

	private static final String FILE_NAME = "/ferryman.properties";

	private static final String TELEGRAM_APIKEY_KEY = "telegram.apikey";
	private static final String TELEGRAM_APIHASH_KEY = "telegram.apihash";
	private static final String TELEGRAM_PHONENUMBER_KEY = "telegram.phonenumber";

	private static final String TELEGRAM_AUTH_PATH_KEY = "telegram.auth.path";
	private static final String TELEGRAM_LOG_PATH_KEY = "telegram.log.path";

	private static final String DIFFERENCES_DATA_BOT_ID_KEY = "differences.data.bot.id";
	private static final String DIFFERENCES_DATA_PTS_KEY = "differences.data.pts";
	private static final String DIFFERENCES_DATA_DATE_KEY = "differences.data.date";
	private static final String DIFFERENCES_DATA_SEQ_KEY = "differences.data.seq";

	private static final String GOLOS_HOST_KEY = "golos.host";

	private static final String MAX_SIZE_CHANNEL_KEY = "max.size.channel";
	private static final String MAX_SIZE_ACCOUNT_KEY = "max.size.account";

	private static final String MIN_SIZE_MERGE_MESSAGES_KEY = "min.size.merge.messages";

	private static final String TELEGRAM_HOST_KEY = "telegram.host";

	private static final String PERIOD_SEND_GOLOS_MESSAGE_KEY = "period.send.golos.messages";

	private static final String TIMEOUT_EXECUTION_COMMAND_GOLOS_KEY = "timeout.execution.command.golos";

	private static final String MAX_SIZE_GOLOS_POST_KEY = "max.size.golos.post";
	private static final String MAX_SIZE_MESSAGES_KEY = "max.size.messages";

	private static final String INTERVAL_RESTART_CONNECT_TELEGRAM_KEY = "interval.restart.connect.telegram";
	private static final String INTERVAL_AUTO_SEND_MESSAGE_KEY = "interval.auto.send.message";
	private static final String INTERVAL_GET_STATUS_BOT_KEY = "interval.get.status.bot";

	private static final String BOT_ID_KEY = "bot.id";
	private static final String BOT_NAME_KEY = "bot.name";

	private static Config INSTANCE;
	private Configuration configuration;
	private URL url;

	private int telegramApikey;
	private String telegramApihash;
	private String telegramPhonenumber;

	private String golosHost;

	private int maxSizeChannel;
	private int maxSizeAccount;

	private int differencesDataBotId;
	private int differencesDataPts;
	private int differencesDataDate;
	private int differencesDataSeq;

	private int minSizeMergeMessages;

	private String telegramHost;

	private int periodSendGolosMessages;

	private int timeoutExecutionCommandGolos;

	private int maxSizeGolosPost;

	private String telegramAuthPath;
	private String telegramLogPath;

	private int maxSizeMessages;

	private int intervalRestartConnectTelegram;
	private int intervalAutoSendMessage;
	private int intervalGetStatusBot;

	private int botId;
	private String botName;

	/**
	 * @return экземпляр конфига
	 */
	public static Config getConfig() {
		if (INSTANCE == null) {
			INSTANCE = new Config();
		}
		return INSTANCE;
	}

	private Config() {
		loadConfig();
	}

	/**
	 * @return перезагрузить конфиг
	 */
	public void reload() {
		loadConfig();
	}

	/**
	 * Загрузка конфига
	 */
	private void loadConfig() {
		String propertiesFile = null;
		String property = "ferryman.properties";
		try {
			propertiesFile = System.getProperty(property);
		} catch (Exception e) {
			LOG.warn("FATAL: can't load System property: " + property);
		}

		try {

			if (StringUtils.isBlank(propertiesFile)) {
				url = Config.class.getResource(FILE_NAME);
			} else {
				url = new File(propertiesFile).toURI().toURL();
			}
			Parameters params = new Parameters();
			FileBasedConfigurationBuilder<FileBasedConfiguration> builder = new FileBasedConfigurationBuilder<FileBasedConfiguration>(
					PropertiesConfiguration.class).configure(params.properties().setURL(url));
			builder.setAutoSave(true);
			configuration = builder.getConfiguration();
			init();
		} catch (Exception e) {
			throw new RuntimeException("FATAL: can't load config from: " + FILE_NAME, e);
		}
	}

	/**
	 * Иницилизация параметров конфига
	 */
	private void init() {
		setDifferencesDataBotId(configuration.getInt(DIFFERENCES_DATA_BOT_ID_KEY));
		setDifferencesDataDate(configuration.getInt(DIFFERENCES_DATA_DATE_KEY));
		setDifferencesDataPts(configuration.getInt(DIFFERENCES_DATA_PTS_KEY));
		setDifferencesDataSeq(configuration.getInt(DIFFERENCES_DATA_SEQ_KEY));

		setTelegramApikey(configuration.getInt(TELEGRAM_APIKEY_KEY));
		setTelegramApihash(configuration.getString(TELEGRAM_APIHASH_KEY));
		setTelegramPhonenumber(configuration.getString(TELEGRAM_PHONENUMBER_KEY));
		setTelegramAuthPath(configuration.getString(TELEGRAM_AUTH_PATH_KEY));
		setTelegramLogPath(configuration.getString(TELEGRAM_LOG_PATH_KEY));

		setGolosHost(configuration.getString(GOLOS_HOST_KEY));

		setMinSizeMergeMessages(configuration.getInt(MIN_SIZE_MERGE_MESSAGES_KEY));
		setTelegramHost(configuration.getString(TELEGRAM_HOST_KEY));

		setPeriodSendGolosMessages(configuration.getInt(PERIOD_SEND_GOLOS_MESSAGE_KEY));
		setTimeoutExecutionCommandGolos(configuration.getInt(TIMEOUT_EXECUTION_COMMAND_GOLOS_KEY));

		setMaxSizeGolosPost(configuration.getInt(MAX_SIZE_GOLOS_POST_KEY));
		setMaxSizeMessages(configuration.getInt(MAX_SIZE_MESSAGES_KEY));

		setMaxSizeChannel(configuration.getInt(MAX_SIZE_CHANNEL_KEY));
		setMaxSizeAccount(configuration.getInt(MAX_SIZE_ACCOUNT_KEY));

		setIntervalRestartConnectTelegram(configuration.getInt(INTERVAL_RESTART_CONNECT_TELEGRAM_KEY));
		setIntervalAutoSendMessage(configuration.getInt(INTERVAL_AUTO_SEND_MESSAGE_KEY));
		setIntervalGetStatusBot(configuration.getInt(INTERVAL_GET_STATUS_BOT_KEY));

		setBotId(configuration.getInt(BOT_ID_KEY));
		setBotName(configuration.getString(BOT_NAME_KEY));
	}

	/**
	 * Получить telegram Apikey
	 * 
	 * @return api key
	 */
	public int getTelegramApikey() {
		return telegramApikey;
	}

	/**
	 * Получить telegram Apihash
	 * 
	 * @return api hash
	 */
	public String getTelegramApihash() {
		return telegramApihash;
	}

	/**
	 * Получить номер телефона на кого зарегистрирован telegram
	 * 
	 * @return номер телефона
	 */
	public String getTelegramPhonenumber() {
		return telegramPhonenumber;
	}

	/**
	 * Получить начальные данные bot Id
	 * 
	 * @return bot Id
	 */
	public int getDifferencesDataBotId() {
		return differencesDataBotId;
	}

	/**
	 * Получить начальные данные pts
	 * 
	 * @return pts
	 */
	public int getDifferencesDataPts() {
		return differencesDataPts;
	}

	/**
	 * Получить начальные данные date
	 * 
	 * @return date
	 */
	public int getDifferencesDataDate() {
		return differencesDataDate;
	}

	/**
	 * Получить начальные данные seq
	 * 
	 * @return seq
	 */
	public int getDifferencesDataSeq() {
		return differencesDataSeq;
	}

	/**
	 * Получить url публичной ноды голоса
	 * 
	 * @return url хоста
	 */
	public String getGolosHost() {
		return golosHost;
	}

	/**
	 * Получить необходимое минимальное количество сообщений для отправки в
	 * голос
	 * 
	 * @return количество сообщений
	 */
	public int getMinSizeMergeMessages() {
		return minSizeMergeMessages;
	}

	/**
	 * Получить хостинг телеграма
	 * 
	 * @return хостинг телеграма
	 */
	public String getTelegramHost() {
		return telegramHost;
	}

	/**
	 * Получить период отправки сообщений в Голос (в минутах)
	 * 
	 * @return период отправки
	 */
	public int getPeriodSendGolosMessages() {
		return periodSendGolosMessages;
	}

	/**
	 * Получить период между повторным обращением к голосу (в секундах)
	 * 
	 * @return период обращения
	 */
	public int getTimeoutExecutionCommandGolos() {
		return timeoutExecutionCommandGolos;
	}

	/**
	 * Получить максимальный размер поста в голосе
	 * 
	 * @return максимальный размер
	 */
	public int getMaxSizeGolosPost() {
		return maxSizeGolosPost;
	}

	/**
	 * Получить максимальное количество каналов на одного пользогвателя
	 * 
	 * @return максимальное количество каналов
	 */
	public int getMaxSizeChannel() {
		return maxSizeChannel;
	}

	/**
	 * Получить максимальное количество аккаунтов на одного пользогвателя
	 * 
	 * @return максимальное количество аккаунтов
	 */
	public int getMaxSizeAccount() {
		return maxSizeAccount;
	}

	/**
	 * Получить путь до папки авторизации
	 * 
	 * @return файла авторизации
	 */
	public String getTelegramAuthPath() {
		return telegramAuthPath;
	}

	/**
	 * Получить путь до папки логов
	 * 
	 * @return файла авторизации
	 */
	public String getTelegramLogPath() {
		return telegramLogPath;
	}

	/**
	 * Получить максимальный размер для сообщений в посте
	 * 
	 * @return файла авторизации
	 */
	public int getMaxSizeMessages() {
		return maxSizeMessages;
	}

	/**
	 * Получить интервал перезагрузки telegramm соедминения в минутах
	 * 
	 * @return интервал
	 */
	public int getIntervalRestartConnectTelegram() {
		return intervalRestartConnectTelegram;
	}

	/**
	 * Получить интервал автоотправки сообщений в минутах
	 * 
	 * @return интервал
	 */
	public int getIntervalAutoSendMessage() {
		return intervalAutoSendMessage;
	}

	/**
	 * Получить интервал получения статуса бота в минутах
	 * 
	 * @return интервал
	 */
	public int getIntervalGetStatusBot() {
		return intervalGetStatusBot;
	}

	/**
	 * Получить идентификатор бота
	 * 
	 * @return идентификатор
	 */
	public int getBotId() {
		return botId;
	}

	/**
	 * Получить имя бота
	 * 
	 * @return идентификатор
	 */
	public String getBotName() {
		return botName;
	}


	public void setDifferencesDataBotId(int differencesDataBotId) {
		this.differencesDataBotId = differencesDataBotId;
	}

	public void setDifferencesDataPts(int differencesDataPts) {
		this.differencesDataPts = differencesDataPts;
	}

	public void setDifferencesDataDate(int differencesDataDate) {
		this.differencesDataDate = differencesDataDate;
	}

	public void setDifferencesDataSeq(int differencesDataSeq) {
		this.differencesDataSeq = differencesDataSeq;
	}

	private void setTelegramApikey(int apikey) {
		this.telegramApikey = apikey;
	}

	private void setTelegramApihash(String apihash) {
		this.telegramApihash = apihash;
	}

	private void setTelegramPhonenumber(String phonenumber) {
		this.telegramPhonenumber = phonenumber;
	}

	private void setGolosHost(String golosHost) {
		this.golosHost = golosHost;
	}

	private void setMinSizeMergeMessages(int minSizeMergeMessages) {
		this.minSizeMergeMessages = minSizeMergeMessages;
	}

	private void setTelegramHost(String telegramHost) {
		this.telegramHost = telegramHost;
	}

	private void setPeriodSendGolosMessages(int periodSendGolosMessages) {
		this.periodSendGolosMessages = periodSendGolosMessages;
	}

	private void setTimeoutExecutionCommandGolos(int timeoutExecutionCommandGolos) {
		this.timeoutExecutionCommandGolos = timeoutExecutionCommandGolos;
	}

	private void setMaxSizeGolosPost(int maxSizeGolosPost) {
		this.maxSizeGolosPost = maxSizeGolosPost;
	}

	private void setMaxSizeChannel(int maxSizeChannel) {
		this.maxSizeChannel = maxSizeChannel;
	}

	private void setMaxSizeAccount(int maxSizeAccount) {
		this.maxSizeAccount = maxSizeAccount;
	}

	private void setTelegramAuthPath(String telegramAuthPath) {
		this.telegramAuthPath = telegramAuthPath;
	}

	private void setTelegramLogPath(String telegramLogPath) {
		this.telegramLogPath = telegramLogPath;
	}

	private void setMaxSizeMessages(int maxSizeMessages) {
		this.maxSizeMessages = maxSizeMessages;
	}

	private void setIntervalRestartConnectTelegram(int intervalRestartConnectTelegram) {
		this.intervalRestartConnectTelegram = intervalRestartConnectTelegram;
	}

	private void setIntervalAutoSendMessage(int intervalAutoSendMessage) {
		this.intervalAutoSendMessage = intervalAutoSendMessage;
	}

	private void setIntervalGetStatusBot(int intervalGetStatusBot) {
		this.intervalGetStatusBot = intervalGetStatusBot;
	}

	private void setBotId(int botId) {
		this.botId = botId;
	}

	private void setBotName(String botName) {
		this.botName = botName;
	}
}
