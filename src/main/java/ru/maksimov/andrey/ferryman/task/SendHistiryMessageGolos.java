package ru.maksimov.andrey.ferryman.task;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.maksimov.andrey.ferryman.structure.Account;
import ru.maksimov.andrey.ferryman.structure.ChatEntity;
import ru.maksimov.andrey.ferryman.structure.MessageEntity;

/**
 * Отправка архивных сообщений
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class SendHistiryMessageGolos extends BaseSendMessageGolos {

	private static final Logger LOG = LogManager.getLogger(SendHistiryMessageGolos.class);

	public void sendMessage(Set<MessageEntity> messages, Integer channelId, String chatTitle, String channelUsername, Account account) {
		LOG.info("Start histiry sendMessage with params messages size: " + messages.size() + " channelId:" + channelId + " chatTitle:" + chatTitle + " messageId:" + account.getMessageId());
		Map<Integer, Set<MessageEntity>> channelId2Messages = new HashMap<Integer, Set<MessageEntity>>();
		channelId2Messages.put(channelId, messages);
		Map<Integer, ChatEntity> channelId2Chat =  new HashMap<Integer, ChatEntity>();
		ChatEntity chatEntity = new ChatEntity();
		chatEntity.setTitle(chatTitle);
		chatEntity.setUsername(channelUsername);
		channelId2Chat.put(channelId, chatEntity);
		sendMessage(channelId2Messages, channelId2Chat, Collections.singleton(account));
		LOG.info("Stop histiry sendMessage");
	}
}
