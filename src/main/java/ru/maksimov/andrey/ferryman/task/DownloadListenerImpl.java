package ru.maksimov.andrey.ferryman.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.api.engine.file.DownloadListener;
import org.telegram.api.engine.file.downloader.DownloadTask;

/**
 * Слушатель загрузки  {@link DownloadListener}
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class DownloadListenerImpl implements DownloadListener {

	private static final Logger LOG = LogManager.getLogger(DownloadListenerImpl.class);

	private int percent;
	private int downloadedSize;
	private DownloadTask task;

	@Override
	public void onPartDownloaded(int percent, int downloadedSize) {
		this.percent = percent;
		this.downloadedSize = downloadedSize;
	}

	@Override
	public void onDownloaded(DownloadTask task) {
		this.task = task;

	}

	@Override
	public void onFailed() {
		LOG.error("Unable download file.");
	}

	public int getPercent() {
		return percent;
	}

	public int getDownloadedSize() {
		return downloadedSize;
	}

	public DownloadTask getTask() {
		return task;
	}

}
