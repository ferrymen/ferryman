package ru.maksimov.andrey.ferryman.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import ru.maksimov.andrey.ferryman.structure.LoginKey;

/**
 * Структура для отправки сообщений в голос
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class Compilation {

	private String message;
	private String head;

	List<LoginKey> loginKeys = new ArrayList<LoginKey>();

	public Compilation() {
	}

	public Compilation(String message, String head) {
		this.message = message;
		this.head = head;
	}

	public String getMessage() {
		return message;
	}

	public String getHead() {
		return head;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public List<LoginKey> getLoginKeys() {
		return loginKeys;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
