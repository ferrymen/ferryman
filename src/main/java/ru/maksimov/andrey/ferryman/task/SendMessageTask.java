package ru.maksimov.andrey.ferryman.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.maksimov.andrey.ferryman.config.Config;
import ru.maksimov.andrey.ferryman.service.BotService;
import ru.maksimov.andrey.ferryman.structure.Status;
import ru.maksimov.andrey.golos4j.exception.BusinessException;

/**
 * Задача для тогого что бы бот не спал и не терял связь с сервером (косяк
 * библиотеки)
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class SendMessageTask implements Runnable {

	private static final Logger LOG = LogManager.getLogger(SendMessageTask.class);
	private BotService botService;
	private Config config = null;

	@Override
	public void run() {
		if (botService == null) {
			return;
		}
		if (config == null) {
			config = Config.getConfig();
		}

		int countReboot = botService.getCountReboot();
		LOG.info("Start reset api, countReboot " + countReboot);
		if (countReboot > (config.getIntervalRestartConnectTelegram() / config.getIntervalGetStatusBot())) {
			if (Status.START == botService.getStatus() && botService.getStatusApi() == 0) {
				LOG.info("Start stop api");
			//	botService.doStop();
			} else {
				LOG.warn("Used api " + botService.getStatusApi());
			}
			if (Status.START != botService.getStatus()) {
				LOG.info("Start start api");
				botService.doRun();
				countReboot = 0;
			}
		} else {
			countReboot++;
		}
		botService.setCountReboot(countReboot);
		LOG.info("End reset api, countReboot " + countReboot);
		LOG.info("Start get status bot, countGetStatusBot ");
		if (Status.START == botService.getStatus()) {
			LOG.info("Start get status bot");
			try {
				botService.getStatusBot(false);
			} catch (BusinessException e) {
				LOG.warn("Unable get status bot " + e.getMessage(), e);
			}
		} else {
			LOG.warn("Used get status bot. Status is " + botService.getStatus());
		}
		LOG.info("End get status bot, countGetStatusBot ");

		int countAutoSendMessage = botService.getCountAutoSendMessage();
		LOG.info("Start auto send message, countAutoSendMessage " + countAutoSendMessage);
		if (countAutoSendMessage > (config.getIntervalAutoSendMessage() / config.getIntervalGetStatusBot())) {
			if (Status.START == botService.getStatus()) {
				LOG.info("Start send message");
				try {
					botService.autoSendHistory();
				} catch (BusinessException e) {
					LOG.warn("Unable auto send message " + e.getMessage(), e);
				}
				countAutoSendMessage = 0;
			} else {
				LOG.warn("Used send message Status is " + botService.getStatus());
				countAutoSendMessage++;
			}
		} else {
			countAutoSendMessage++;
		}
		botService.setCountAutoSendMessage(countAutoSendMessage);
		LOG.info("End auto send message, countAutoSendMessage " + countAutoSendMessage);
	}

	public void setBotService(BotService botService) {
		this.botService = botService;
	}

}
