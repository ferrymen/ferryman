package ru.maksimov.andrey.ferryman.task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;
import org.telegram.api.engine.file.DownloadListener;
import org.telegram.api.engine.file.Downloader;
import org.telegram.api.file.location.TLFileLocation;
import org.telegram.api.input.filelocation.TLInputFileLocation;

import ru.maksimov.andrey.ferryman.config.Config;
import ru.maksimov.andrey.ferryman.image.CloudinaryImageService;
import ru.maksimov.andrey.ferryman.image.ImageService;
import ru.maksimov.andrey.ferryman.image.XchartImageService;
import ru.maksimov.andrey.ferryman.structure.Account;
import ru.maksimov.andrey.ferryman.structure.ChatEntity;
import ru.maksimov.andrey.ferryman.structure.LoginKey;
import ru.maksimov.andrey.ferryman.structure.MediaEntity;
import ru.maksimov.andrey.ferryman.structure.MessageEntity;
import ru.maksimov.andrey.golos4j.api.method.BroadcastTransactionSynchronous;
import ru.maksimov.andrey.golos4j.api.method.GetConfig;
import ru.maksimov.andrey.golos4j.api.method.GetDynamicGlobalProperties;
import ru.maksimov.andrey.golos4j.dto.ConfigDto;
import ru.maksimov.andrey.golos4j.dto.DynamicGlobalPropertiesDto;
import ru.maksimov.andrey.golos4j.dto.ErrorDto;
import ru.maksimov.andrey.golos4j.dto.api.GetBroadcastTransactionSynchronousDto;
import ru.maksimov.andrey.golos4j.dto.api.GetConfigDto;
import ru.maksimov.andrey.golos4j.dto.api.GetDynamicGlobalPropertiesDto;
import ru.maksimov.andrey.golos4j.dto.operation.BaseOperation;
import ru.maksimov.andrey.golos4j.dto.operation.CommentDto;
import ru.maksimov.andrey.golos4j.dto.operation.CommentOptionsDto;
import ru.maksimov.andrey.golos4j.dto.operation.extension.CommentOptionsExtension;
import ru.maksimov.andrey.golos4j.dto.operation.extension.CommentPayoutBeneficiaries;
import ru.maksimov.andrey.golos4j.dto.param.Asset;
import ru.maksimov.andrey.golos4j.dto.param.Asset.AssetSymbolType;
import ru.maksimov.andrey.golos4j.dto.param.BeneficiaryRouteTypeDto;
import ru.maksimov.andrey.golos4j.dto.transaction.BaseTransactionDto;
import ru.maksimov.andrey.golos4j.exception.BusinessException;
import ru.maksimov.andrey.golos4j.socket.ServiceWebSocket;
import ru.maksimov.andrey.golos4j.util.TransactionUtil;
import ru.maksimov.andrey.golos4j.util.Util;

/**
 * Базовый класс для отправки сообщений а голос
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class BaseSendMessageGolos {

	private static final Logger LOG = LogManager.getLogger(BaseSendMessageGolos.class);

	private static final String DEFAULT_HEAD = "Трансляция новостей";

	private static final String SOURCE = "Источник";

	private static final String SPACE_SIGN = " ";
	public static final String NL_SIGN = "\n";
	public static final String SLASH_SIGN = "/";

	public static final String OPEN_DIV_TEG = "<div>";
	public static final String OPEN_DIV_LEFT_TEG = "<div class='pull-left'>";
	public static final String CLOSE_DIV_TEG = "</div>";

	public static final String OPEN_SPAN_TEG = "<span>";
	public static final String CLOSE_SPAN_TEG = "</span>";

	public static final String OPEN_A_TEG = "<a href='";
	public static final String OPEN_A_END_TEG = "'>";
	public static final String CLOSE_A_TEG = "</a>";

	public static final String OPEN_PRE_TEG = "<pre>";
	public static final String CLOSE_PRE_TEG = "</pre>";

	public static final String OPEN_IMG_TEG = "<img src='";
	public static final String CLOSE_IMG_TEG = "'>";
	public static final String BR_TEG = "<br>";

	private static final String COMPILATION_TAG = "ru--podborka";
	static final String TAG = "news-aggregator";

	private static final String OPEN_EM_TEG = "<em>";
	private static final String CLOSE_EM_TEG = "</em>";

	private Downloader downloader;

	private Config config = null;

	private boolean isBeneficiary;

	private ImageService cloudinaryImageService = new CloudinaryImageService();
	private ImageService xchartImageService = new XchartImageService();

	protected void sendMessage(Map<Integer, Set<MessageEntity>> channelId2Messages,
			Map<Integer, ChatEntity> channelId2Chat, Set<Account> accounts) {
		LOG.info("Start sendMessage");
		List<Compilation> compilations = generator(channelId2Messages, channelId2Chat, accounts);
		for (Compilation compilation : compilations) {
			LOG.info(compilation);
			for (LoginKey loginKey : compilation.getLoginKeys()) {
				send(compilation.getMessage(), compilation.getHead(), loginKey.getLogin(), loginKey.getKey());
			}
		}
		LOG.info("Stop sendMessage");
	}

	protected Config getConfig() {
		if (config == null) {
			config = Config.getConfig();
		}
		return config;
	}

	protected void setConfig(Config config) {
		this.config = config;
	}

	public void setBeneficiary(boolean isBeneficiary) {
		this.isBeneficiary = isBeneficiary;
	}

	private List<Compilation> generator(Map<Integer, Set<MessageEntity>> channelId2Messages,
			Map<Integer, ChatEntity> channelId2Chat, Set<Account> accounts) {
		Map<Integer, Set<LoginKey>> channelId2Users = new HashMap<Integer, Set<LoginKey>>();
		for (Account account : accounts) {
			boolean isCorrect = (StringUtils.isNoneBlank(account.getLogin())
					&& StringUtils.isNoneBlank(account.getPrivateKey()) && account.getChannelId() != null);
			if (isCorrect) {
				Set<LoginKey> loginKeys = channelId2Users.get(account.getChannelId());
				if (loginKeys == null) {
					loginKeys = new HashSet<LoginKey>();
					channelId2Users.put(account.getChannelId(), loginKeys);
				}
				LoginKey loginKey = new LoginKey();
				loginKey.setKey(account.getPrivateKey());
				loginKey.setLogin(account.getLogin());
				loginKeys.add(loginKey);
			}
		}
		List<Compilation> compilations = new ArrayList<Compilation>(channelId2Messages.size());
		for (Integer channelId : channelId2Messages.keySet()) {
			ChatEntity chatEntity = channelId2Chat.get(channelId);
			if (chatEntity != null) {
				Compilation compilation = new Compilation();
				String title = chatEntity.getTitle();
				if (title == null || title.isEmpty()) {
					continue;
				}
				compilation.setHead(chatEntity.getTitle());
				Set<LoginKey> logiKeys = channelId2Users.get(channelId);
				if (logiKeys == null || logiKeys.isEmpty()) {
					continue;
				}
				compilation.getLoginKeys().addAll(logiKeys);
				compilations.add(compilation);
				StringBuilder messages = new StringBuilder();
				Set<MessageEntity> setMessages = channelId2Messages.get(channelId);
				List<MessageEntity> listMessages = new ArrayList<MessageEntity>(setMessages);
				Comparator<MessageEntity> byId = (MessageEntity o1, MessageEntity o2) -> o1.getId()
						.compareTo(o2.getId());
				Collections.sort(listMessages, byId);
				int countMessage = 0;
				for (MessageEntity message : listMessages) {
					if (countMessage > 2) {
						break;
					}
					if (StringUtils.isBlank(message.getText()) && message.getMedia() == null) {
						continue;
					}

					// Разделяем пост если он подходит к границе 64rb
					if (messages.toString().length() * 2 > getConfig().getMaxSizeGolosPost()) {
						compilation.setMessage(messages.toString());
						compilation = new Compilation();
						compilations.add(compilation);
						compilation.setHead(chatEntity.getTitle());
						compilation.getLoginKeys().addAll(logiKeys);
						messages = new StringBuilder();
						countMessage++;
					}
					append(message.getText(), messages);

					append(OPEN_DIV_TEG, messages);
					MediaEntity media = message.getMedia();
					if (media != null) {
						// url
						append(OPEN_EM_TEG, messages);
						appendUrl(media.getUrl(), media.getTitle(), messages);
						append(OPEN_DIV_TEG, messages);
						appendSpan(media.getDescription(), messages);
						append(CLOSE_DIV_TEG, messages);
						append(CLOSE_EM_TEG, messages);
						TLFileLocation location = media.getLocation();
						if (downloader != null && location != null) {
							TLInputFileLocation fileLocation = new TLInputFileLocation();
							fileLocation.setLocalId(location.getLocalId());
							fileLocation.setSecret(location.getSecret());
							fileLocation.setVolumeId(location.getVolumeId());
							int dcId = location.getDcId();
							int size = media.getSize();

							DownloadListener downloadListener = new DownloadListenerImpl();
							List<Byte> file = new ArrayList<Byte>();
							int taskId = downloader.requestTask(dcId, fileLocation, size, file, downloadListener);
							while (true) {
								int status = downloader.getTaskState(taskId);
								if (status == Downloader.FILE_COMPLETED) {
									break;
								} else if (status >= Downloader.FILE_FAILURE) {
									LOG.warn("Unable get image status is FAILURE");
									break;
								}
							}

							if (file != null && !file.isEmpty()) {
								String imageUrl = null;
								try {
									imageUrl = xchartImageService.getImageUrl(file);
								} catch (BusinessException e) {
									LOG.warn("Unable get image  message xchartImageService: " + e.getMessage(), e);
									e.printStackTrace();
								}
								if (imageUrl == null) {
									try {
										imageUrl = cloudinaryImageService.getImageUrl(file);
									} catch (BusinessException e) {
										LOG.warn("Unable get image  message cloudinaryImageService: " + e.getMessage(),
												e);
										e.printStackTrace();
									}
								}
								appendImage(imageUrl, messages);
							} else {
								LOG.warn("Image file is empty unable load image localId " + location.getLocalId());
							}
						}
					}
					messages.append(CLOSE_DIV_TEG);
					messages.append(NL_SIGN);
					if (message.getId() != null) {
						String url = getConfig().getTelegramHost() + chatEntity.getUsername() + SLASH_SIGN
								+ message.getId();
						appendUrl(url, SOURCE, messages);
					}
					append(BR_TEG, messages);
					append(BR_TEG, messages);
					messages.append(NL_SIGN);
				}
				compilation.setMessage(messages.toString());

			} else {
				LOG.warn("Unable send messages Chat is null channelId " + channelId);
			}
		}
		return compilations;
	}

	private void appendUrl(String url, String urlName, StringBuilder aggregator) {
		if (url != null && urlName != null) {
			aggregator.append(OPEN_A_TEG);
			aggregator.append(url);
			aggregator.append(OPEN_A_END_TEG);
			aggregator.append(urlName);
			aggregator.append(CLOSE_A_TEG);
			aggregator.append(NL_SIGN);
		}
	}

	private void appendImage(String image, StringBuilder aggregator) {
		if (image != null) {
			aggregator.append(OPEN_PRE_TEG);
			aggregator.append(OPEN_IMG_TEG);
			aggregator.append(image);
			aggregator.append(CLOSE_IMG_TEG);
			aggregator.append(CLOSE_PRE_TEG);
			aggregator.append(NL_SIGN);
		}
	}

	private void appendSpan(String value, StringBuilder aggregator) {
		if (value != null) {
			aggregator.append(OPEN_SPAN_TEG);
			aggregator.append(value);
			aggregator.append(CLOSE_SPAN_TEG);
			aggregator.append(NL_SIGN);
		}
	}

	private void append(String vavlue, StringBuilder aggregator) {
		if (vavlue != null) {
			aggregator.append(vavlue);
			aggregator.append(NL_SIGN);
		}
	}

	public void setDownloader(Downloader downloader) {
		this.downloader = downloader;
	}

	private void send(String message, String head, String login, String key) {
		try {
			LOG.info("Start BaseSendMessageGolos");
			if (StringUtils.isNotBlank(message)) {
				CommentDto commentDto = new CommentDto();
				String title = DEFAULT_HEAD + SPACE_SIGN;
				if (StringUtils.isNotBlank(head)) {
					title += head;
				}
				String permlink = Util.title2Permlink(title);
				String tag = Util.text2Tag(head);
				if (tag.length() == 0) {
					tag += "news";
				}

				if ("01234567890".contains(tag.substring(0, 1))) {
					tag = "te" + tag;
				}
				Map<String, List<String>> key2value = new HashMap<String, List<String>>();
				List<String> tags = new ArrayList<String>();
				tags.add(tag);
				tags.add(COMPILATION_TAG);

				List<String> images = new ArrayList<String>();
				key2value.put(CommentDto.TAGS_KEY, tags);
				key2value.put(CommentDto.IMAGE_KEY, images);
				commentDto.setAuthor(login);
				commentDto.setBody(message);
				commentDto.setJsonMetadata(key2value);
				commentDto.setParentAuthor("");
				commentDto.setParentPermlink(TAG);
				commentDto.setPermlink(permlink);
				commentDto.setTitle(title);

				List<BaseOperation> operations = new ArrayList<BaseOperation>();
				operations.add(commentDto);
				if (isBeneficiary) {
					CommentOptionsDto commentOptionsDto = new CommentOptionsDto();
					operations.add(commentOptionsDto);
					commentOptionsDto.setAllowCurationRewards(true);
					commentOptionsDto.setAllowVotes(true);
					commentOptionsDto.setAuthor(login);
					Asset maxAcceptedPayout = new Asset(1000000, AssetSymbolType.GBG);
					commentOptionsDto.setMaxAcceptedPayout(maxAcceptedPayout);
					// 100%
					commentOptionsDto.setPercentSteemDollars((short) 10000);
					commentOptionsDto.setPermlink(permlink);
					List<CommentOptionsExtension> extensions = new ArrayList<CommentOptionsExtension>();
					commentOptionsDto.setExtensions(extensions);
					CommentPayoutBeneficiaries beneficiaries = new CommentPayoutBeneficiaries();
					extensions.add(beneficiaries);
					List<BeneficiaryRouteTypeDto> beneficiariesRoute = new ArrayList<BeneficiaryRouteTypeDto>();
					beneficiaries.setBeneficiaries(beneficiariesRoute);
					BeneficiaryRouteTypeDto beneficiaryRouteTypeDto1 = new BeneficiaryRouteTypeDto();
					beneficiariesRoute.add(beneficiaryRouteTypeDto1);
					beneficiaryRouteTypeDto1.setAccount("netfriend");
					beneficiaryRouteTypeDto1.setWeight((short) 1000);
					BeneficiaryRouteTypeDto beneficiaryRouteTypeDto2 = new BeneficiaryRouteTypeDto();
					beneficiariesRoute.add(beneficiaryRouteTypeDto2);
					beneficiaryRouteTypeDto2.setAccount("vik");
					beneficiaryRouteTypeDto2.setWeight((short) 1000);
				}

				int count = 0;
				boolean isSeand = false;
				while (!isSeand) {
					try {
						count++;
						GetBroadcastTransactionSynchronousDto gb = sendPost(operations, key);
						LOG.info("result broadcast transaction synchronous: " + gb);
						ErrorDto error = gb.getError();
						if (error == null) {
							isSeand = true;
						}
					} catch (Exception e) {
						LOG.warn("result broadcast transaction synchronous: " + e.getMessage(), e);
						if (count > 5) {
							break;
						}
					}
				}
			}
			LOG.info("Stop BaseSendMessageGolos");
		} catch (Exception e) {
			LOG.warn("Unable send message:" + e.getMessage(), e);
		}
		try {
			Thread.sleep(getConfig().getTimeoutExecutionCommandGolos() * 1000);
		} catch (InterruptedException e) {
			LOG.warn("result broadcast transaction synchronous: " + e.getMessage(), e);
		}
	}

	protected GetBroadcastTransactionSynchronousDto sendPost(List<BaseOperation> operations, String key)
			throws Exception {
		int id = 2;
		String host = getConfig().getGolosHost();
		GetDynamicGlobalPropertiesDto getDynamicGlobalPropertiesDto = getDynamicGlobalProperties(host);
		Thread.sleep(getConfig().getTimeoutExecutionCommandGolos() * 1000);
		DynamicGlobalPropertiesDto dynamicGlobalPropertiesDto = getDynamicGlobalPropertiesDto.getResults();

		BaseTransactionDto baseTransactionDto = new BaseTransactionDto();
		long headBlockNumber = dynamicGlobalPropertiesDto.getHeadBlockNumber();
		String headBlockId = dynamicGlobalPropertiesDto.getHeadBlockId();
		int refBlockNum = TransactionUtil.long2Last2Byte(headBlockNumber);
		baseTransactionDto.setRefBlockNum(refBlockNum);
		long refBlockPrefix = TransactionUtil.hexString2Long(headBlockId, 4);
		baseTransactionDto.setRefBlockPrefix(refBlockPrefix);
		Date time = dynamicGlobalPropertiesDto.getTime();
		Date expiration = Util.addTime(time, BaseTransactionDto.DEFAULT_EXPIRATION_TIME * 3);
		baseTransactionDto.setExpiration(expiration);
		baseTransactionDto.getOperations().addAll(operations);

		GetConfigDto getConfigDto = getConfig(host);
		Thread.sleep(getConfig().getTimeoutExecutionCommandGolos() * 1000);
		ConfigDto configDto = getConfigDto.getResults();
		String chainId = configDto.getSteemitChainId();
		ECKey postingKey = DumpedPrivateKey.fromBase58(null, key).getKey();
		baseTransactionDto.setSignatures(chainId, postingKey);

		BroadcastTransactionSynchronous broadcastTransactionSynchronous = new BroadcastTransactionSynchronous(id,
				baseTransactionDto);
		GetBroadcastTransactionSynchronousDto gb = ServiceWebSocket.executePost(broadcastTransactionSynchronous,
				GetBroadcastTransactionSynchronousDto.class, host);
		return gb;
	}

	protected static GetDynamicGlobalPropertiesDto getDynamicGlobalProperties(String host) throws Exception {
		LOG.info("Start getDynamicGlobalProperties");
		int id = 2;
		GetDynamicGlobalProperties getDynamicGlobalProperties = new GetDynamicGlobalProperties(id);
		GetDynamicGlobalPropertiesDto getDynamicGlobalPropertiesDto = ServiceWebSocket
				.executePost(getDynamicGlobalProperties, GetDynamicGlobalPropertiesDto.class, host);
		LOG.info("Stop getDynamicGlobalProperties");
		return getDynamicGlobalPropertiesDto;
	}

	protected static GetConfigDto getConfig(String host) throws Exception {
		LOG.info("Start getConfig");
		int id = 2;
		GetConfig getConfig = new GetConfig(id);
		GetConfigDto getConfigDto = ServiceWebSocket.executePost(getConfig, GetConfigDto.class, host);
		LOG.info("Stop getConfig");
		return getConfigDto;
	}

}
