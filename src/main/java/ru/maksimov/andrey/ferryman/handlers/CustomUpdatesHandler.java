package ru.maksimov.andrey.ferryman.handlers;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.api.chat.TLAbsChat;
import org.telegram.api.message.TLAbsMessage;
import org.telegram.api.message.TLMessage;

import org.telegram.api.peer.TLAbsPeer;
import org.telegram.api.peer.TLPeerUser;

import org.telegram.api.update.TLUpdateChannelNewMessage;
import org.telegram.api.update.TLUpdateNewMessage;
import org.telegram.api.updates.TLUpdateShortMessage;
import org.telegram.api.user.TLAbsUser;
import org.telegram.bot.handlers.DefaultUpdatesHandler;

import org.telegram.bot.handlers.interfaces.IChatsHandler;
import org.telegram.bot.handlers.interfaces.IUsersHandler;
import org.telegram.bot.kernel.IKernelComm;
import org.telegram.bot.kernel.differenceparameters.IDifferenceParametersService;
import org.telegram.bot.structure.IUser;

import ru.maksimov.andrey.ferryman.config.Config;
import ru.maksimov.andrey.ferryman.database.DatabaseManagerImpl;
import ru.maksimov.andrey.ferryman.database.entity.StatusBotEntity;
import ru.maksimov.andrey.ferryman.service.BotStatusService;

/**
 * Обработчик обновлений
 * 
 * @author amaksimov
 */
public class CustomUpdatesHandler extends DefaultUpdatesHandler {

	private static final Logger LOG = LogManager.getLogger(CustomUpdatesHandler.class);

	private final DatabaseManagerImpl databaseManager;

	private IUsersHandler usersHandler;
	private IChatsHandler chatsHandler;
	private MessageHandler messageHandler;

	private BotStatusService botStatusService;

	private Config config;

	public CustomUpdatesHandler(IKernelComm kernelComm, IDifferenceParametersService differenceParametersService,
			DatabaseManagerImpl databaseManager) {
		super(kernelComm, differenceParametersService, databaseManager);
		this.databaseManager = databaseManager;
	}

	public void setHandlers(MessageHandler messageHandler, IUsersHandler usersHandler, IChatsHandler chatsHandler) {
		this.chatsHandler = chatsHandler;
		this.usersHandler = usersHandler;
		this.messageHandler = messageHandler;
	}

	@Override
	public void onTLUpdateShortMessageCustom(TLUpdateShortMessage update) {
		if (update.getUserId() == getConfig().getBotId()) {
			notifyStatusBot(update.getMessage(), update.getUserId());
		} else {
			final IUser user = databaseManager.getUserById(update.getUserId());
			if (user != null) {
				LOG.info("Received message from: " + update.getUserId());
				messageHandler.handleMessage(user, update);
			}

		}
	}

	@Override
	public void onTLUpdateNewMessageCustom(TLUpdateNewMessage update) {
		onTLAbsMessageCustom(update.getMessage());
	}

	@Override
	protected void onTLAbsMessageCustom(TLAbsMessage message) {
		if (message instanceof TLMessage) {
			onTLMessage((TLMessage) message);
		} else {
			LOG.debug("Unsupported TLAbsMessage -> " + message.toString());
		}
	}

	@Override
	protected void onTLUpdateChannelNewMessageCustom(TLUpdateChannelNewMessage update) {
		onTLAbsMessageCustom(update.getMessage());
	}

	@Override
	protected void onUsersCustom(List<TLAbsUser> users) {
		usersHandler.onUsers(users);
	}

	@Override
	protected void onChatsCustom(List<TLAbsChat> chats) {
		chatsHandler.onChats(chats);
	}

	/**
	 * Handles TLMessage
	 * 
	 * @param message
	 *            Message to handle
	 */
	private void onTLMessage(@NotNull TLMessage message) {
		if (531197855 != message.getFromId() || 292641120 == message.getFromId()) {
			return;
		} else if (message.getFromId() == getConfig().getBotId() && message.getToId() != null
				&& message.getToId().getId() == botStatusService.getCurrentUserId()) {
			notifyStatusBot(message.getMessage(), message.getFromId());
		}
		LOG.info("onTLMessage: ChatId: " + message.getChatId() + " date: " + message.getDate());
		TLAbsPeer toId = message.getToId();
		if (toId instanceof TLPeerUser) {
			if (message.getFromId() == toId.getId()) {
				return;
			}
			final IUser user = databaseManager.getUserById(message.getFromId());
			if (user != null) {
				this.messageHandler.handleMessage(user, message);
			}
		}
	}

	public void setBotStatusService(BotStatusService botStatusService) {
		this.botStatusService = botStatusService;
	}

	private void notifyStatusBot(String message, int userId) {
		try {
			if (StringUtils.isNotBlank(message)) {
				StatusBotEntity statusBot = new StatusBotEntity();
				statusBot.setId(userId);
				statusBot.setMessage(message);
				statusBot.setUpdatedAt(new Date());
				databaseManager.setStatusBot(statusBot);
			} else {
				LOG.warn("Unable save status bot, messahe is empty");
			}
		} finally {
			botStatusService.notifyStatusBot();
			LOG.info("notifyStatusBot msg " + message);
		}
	}

	private Config getConfig() {
		if (config == null) {
			config = Config.getConfig();
		}
		return config;
	}
}
