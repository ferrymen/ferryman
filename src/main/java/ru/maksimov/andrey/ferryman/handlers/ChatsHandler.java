package ru.maksimov.andrey.ferryman.handlers;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.api.chat.TLAbsChat;
import org.telegram.api.chat.TLChat;
import org.telegram.api.chat.TLChatForbidden;
import org.telegram.api.chat.channel.TLChannel;
import org.telegram.api.chat.channel.TLChannelForbidden;
import org.telegram.bot.handlers.interfaces.IChatsHandler;

import ru.maksimov.andrey.ferryman.database.DatabaseManagerImpl;
import ru.maksimov.andrey.ferryman.structure.ChatEntity;


/**
 * Обработчик чатов (добавление\обновление чата)
 * 
 * @author amaksimov
 */
public class ChatsHandler implements IChatsHandler {

	private static final Logger LOG = LogManager.getLogger(ChatsHandler.class);

	private DatabaseManagerImpl databaseManager;

	public ChatsHandler(DatabaseManagerImpl databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public void onChats(List<TLAbsChat> chats) {
		chats.forEach(this::onAbsChat);
	}

	// ChatEntity
	private void onAbsChat(TLAbsChat chat) {
		if (chat instanceof TLChannel) {
			onChannel((TLChannel) chat);
		} else if (chat instanceof TLChannelForbidden) {
			onChannelForbidden((TLChannelForbidden) chat);
		} else if (chat instanceof TLChat) {
			onChat((TLChat) chat);
		} else if (chat instanceof TLChatForbidden) {
			onChatForbidden((TLChatForbidden) chat);
		} else {
			LOG.warn("Unsupported chat type " + chat);
		}
	}

	private void onChatForbidden(TLChatForbidden chat) {
		onChat(chat.getId(), chat.getTitle());
	}

	private void onChat(TLChat chat) {
		onChat(chat.getId(), chat.getTitle());
	}

	private void onChat(int chatId, String title) {
		AtomicBoolean isUpdating = new AtomicBoolean(true);
		ChatEntity current = actionChat(chatId, isUpdating);
		current.setChannel(false);
		saveChat(current, isUpdating);
	}

	private void onChannelForbidden(TLChannelForbidden channel) {
		AtomicBoolean isUpdating = new AtomicBoolean(true);
		ChatEntity current = actionChat(channel.getId(), isUpdating);
		current.setChannel(true);
		current.setAccessHash(channel.getAccessHash());
		saveChat(current, isUpdating);
	}

	private void onChannel(TLChannel channel) {
		AtomicBoolean isUpdating = new AtomicBoolean(true);
		ChatEntity current = actionChat(channel.getId(), isUpdating);
		current.setChannel(true);
		if (channel.hasAccessHash()) {
			current.setAccessHash(channel.getAccessHash());
		}
		saveChat(current, isUpdating);
	}

	private ChatEntity actionChat(int chatId, AtomicBoolean isUpdating) {
		ChatEntity current = (ChatEntity) databaseManager.getChatById(chatId);
		if (current == null) {
			isUpdating.set(false);
			current = new ChatEntity(chatId);
		}
		return current;
	}

	private void saveChat(ChatEntity chat, AtomicBoolean isUpdating) {
		if (isUpdating.get()) {
			databaseManager.updateChat(chat);
		} else {
			databaseManager.addChat(chat);
		}
	}


}
