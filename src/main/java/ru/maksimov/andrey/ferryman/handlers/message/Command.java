package ru.maksimov.andrey.ferryman.handlers.message;

/**
 * Список команд и сообщений
 * 
 * @author amaksimov
 */
public enum Command {

	//Mainmenu
	MAINMENU_SEND_MESSAGE("^send\\smessage\\s(.*)$", Text.SEND_MESSAGE_SUCCESS, Text.SEND_MESSAGE_FAIL),
	MAINMENU_ACCOUNT_LIST("^account\\slist$", Text.ACCOUNT_LIST_SUCCESS, Text.ACCOUNT_LIST_FAIL),
	MAINMENU_ACCOUNT_SETTINGS("^account\\ssettings\\s(.*)$", Text.ACCOUNT_SETTINGS_SUCCESS, Text.ACCOUNT_SETTINGS_FAIL),
	MAINMENU_HELP("^help$", Text.MAINMENU_HELP, Text.MAINMENU_HELP),
	//Account settings
	ACCOUNT_SETTINGS_ADD_CHANNL("^add\\schannel\\s(.*)$", Text.ADD_CHANNEL_SUCCESS, Text.ADD_CHANNEL_FAIL),
	ACCOUNT_SETTINGS_SET_KEY("^set\\skey\\s(\\w+)$", Text.SET_KEY_SUCCESS, Text.SET_KEY_FAIL),
	ACCOUNT_SETTINGS_SET_MIN_COUNT_POSTS("^set\\smin\\scount\\s(\\d+)$", Text.SET_MIN_COUNT_POSTS_SUCCESS, Text.SET_MIN_COUNT_POSTS_FAIL),
	ACCOUNT_SETTINGS_SET_MAX_COUNT_POSTS("^set\\smax\\scount\\s(\\d+)$", Text.SET_MAX_COUNT_POSTS_SUCCESS, Text.SET_MAX_COUNT_POSTS_FAIL),
	ACCOUNT_SETTINGS_SET_MESSAGE_ID("^set\\smessage\\sid\\s(-{0,1}\\d+)$", Text.SET_MESSAGE_ID_SUCCESS, Text.SET_MESSAGE_ID_FAIL),
	ACCOUNT_SETTINGS_SET_AUTOSEND_MESSAGE("^set\\sautosend\\smessage\\s(true|false)$", Text.SET_AUTOSEND_SUCCESS, Text.SET_AUTOSEND_FAIL),
	ACCOUNT_SETTINGS_DELETE_ACCOUNT("^delete\\saccount$", Text.DELETE_ACCOUNT_SUCCESS, Text.DELETE_ACCOUNT_FAIL),
	ACCOUNT_SETTINGS_GET_STATE("^get\\sstate$", Text.GET_STATE_SUCCESS, Text.GET_STATE_FAIL),

	MAINMENU("^mainmenu$", Text.MAINMENU_SUCCESS, Text.MAINMENU_SUCCESS),
	ACCOUNT_SETTINGS_HELP("^help$", Text.ACCOUNT_HELP, Text.ACCOUNT_HELP);

	private String regex;
	private String  successfulMessage;
	private String  errorMessage;

	Command(String regex, String successfulMessage, String errorMessage) {
		this.regex = regex;
		this.successfulMessage = successfulMessage;
		this.errorMessage = errorMessage;
	}

	public String getRegex() {
		return regex;
	}

	public String getSuccessfulMessage() {
		return successfulMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
