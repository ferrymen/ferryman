package ru.maksimov.andrey.ferryman.handlers.message;

public enum Menu {
	MAINMENU, ACCOUNT;

	/**
	 * Найти пункт меню по строке
	 * 
	 * @param str
	 *            меню
	 * @return пункт меню, если нет совпадений то вернуть главное меню
	 */
	public static Menu find(String str) {
		for (Menu menu : values()) {
			if (menu.name().equalsIgnoreCase(str)) {
				return menu;
			}
		}
		return MAINMENU;

	}
}
