package ru.maksimov.andrey.ferryman.handlers;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.api.user.TLAbsUser;
import org.telegram.api.user.TLUser;
import org.telegram.bot.handlers.interfaces.IUsersHandler;

import ru.maksimov.andrey.ferryman.database.DatabaseManagerImpl;
import ru.maksimov.andrey.ferryman.structure.UserEntity;

/**
 * Обработчик пользователей (добавление\обновление пользователя)
 * 
 * @author amaksimov
 */
public class UsersHandler implements IUsersHandler {

	private static final Logger LOG = LogManager.getLogger(CustomUpdatesHandler.class);

	private static final int MAX_TEMPORALUSERS = 4000;

	private final ConcurrentHashMap<Integer, TLAbsUser> temporalUsers = new ConcurrentHashMap<>();

	private DatabaseManagerImpl databaseManager;

	public UsersHandler(DatabaseManagerImpl databaseManager) {
		this.databaseManager = databaseManager;
	}

	@Override
	public void onUsers(List<TLAbsUser> users) {
		if ((temporalUsers.size() + users.size()) > MAX_TEMPORALUSERS) {
			temporalUsers.clear();
		}
		users.stream().forEach(user -> temporalUsers.put(user.getId(), user));
		users.forEach(this::onUser);
	}

	/**
	 * Add a user to database
	 * 
	 * @param absUser
	 *            User to add
	 */
	private void onUser(@NotNull TLAbsUser absUser) {
		UserEntity currentUser = null;
		UserEntity user = null;
		if (absUser instanceof TLUser) {
			final TLUser tlUser = (TLUser) absUser;
			if (tlUser.isMutualContact()) {
				currentUser = (UserEntity) databaseManager.getUserById(tlUser.getId());
				user = onUserUpdate(currentUser, tlUser);
			} else if (tlUser.isDeleted()) {
				currentUser = (UserEntity) databaseManager.getUserById(tlUser.getId());
				user = onUserDelete(currentUser, tlUser);
			} else if (tlUser.isContact()) {
				currentUser = (UserEntity) databaseManager.getUserById(tlUser.getId());
				user = onUserUpdate(currentUser, tlUser);
			} else if (tlUser.isSelf() || !tlUser.isBot()) {
				currentUser = (UserEntity) databaseManager.getUserById(tlUser.getId());
				user = onUserUpdate(currentUser, tlUser);
			} else if (tlUser.isBot()) {
				currentUser = (UserEntity) databaseManager.getUserById(tlUser.getId());
				user = onUserUpdate(currentUser, tlUser);
			} else {
				LOG.info("Bot received");
			}
		}
		if ((currentUser == null) && (user != null)) {
			databaseManager.addUser(user);
		} else if (user != null) {
			databaseManager.updateUser(user);
		}
	}

	/**
	 * Create User from a delete user
	 * 
	 * @param currentUser
	 *            Current use from database (null if not present)
	 * @param userDeleted
	 *            Delete user from Telegram Server
	 * @return User information
	 */
	private UserEntity onUserDelete(@Nullable UserEntity currentUser, @NotNull TLUser userDeleted) {
		final UserEntity user = actionUser(currentUser, userDeleted);
		user.setUserHash(0L);
		LOG.debug("userdeletedid: " + user.getUserId());
		return user;
	}

	/**
	 * Create User from a update user
	 * 
	 * @param currentUser
	 *            Current use from database (null if not present)
	 * @param userContact
	 *            Contact user from Telegram Server
	 * @return User information
	 */
	private UserEntity onUserUpdate(@Nullable UserEntity currentUser, @NotNull TLUser userAction) {
		final UserEntity user = actionUser(currentUser, userAction);
		user.setUserHash(userAction.getAccessHash());
		LOG.debug("usercontactid: " + user.getUserId());
		return user;
	}

	private UserEntity actionUser(UserEntity currentUser, TLUser userAction) {
		final UserEntity user;
		if (currentUser == null) {
			user = new UserEntity(userAction.getId());
		} else {
			user = new UserEntity(currentUser);
		}
		return user;
	}

}
