package ru.maksimov.andrey.ferryman.handlers.message;

/**
 * Вспомогательный класс с сообщениями
 * 
 * @author amaksimov
 */
public class Text {

	public static String SEND_MESSAGE_SUCCESS = "Cообщения отправлены в Голос ";
	public static String SEND_MESSAGE_FAIL = "Ошибка при отправки сообщений в Голос: ";

	public static String ACCOUNT_LIST_SUCCESS = "Список аккаунтов: ";
	public static String ACCOUNT_LIST_FAIL = "Ошибка при получении список аккаунтов: ";

	public static String ACCOUNT_SETTINGS_SUCCESS = "Настройки аккааунта: ";
	public static String ACCOUNT_SETTINGS_FAIL = "Ошибка при получении настроек аккауна: ";

	private static String HELP_MAINMENU_SEND_MESSAGE = "send message [название канала] - команда для отправки сообщений в голос \n";
	private static String HELP_MAINMENU_ACCOUNT_LIST = "account list - команда показать список аккаунтов \n";
	private static String HELP_MAINMENU_ACCOUNT_SETTINGS = "account settings [логин аккаунта] - команда для в меню настройки аккаунта \n";
	private static String HELP_MAINMENU_HELP = "help - команда помощь \n";

	public static String MAINMENU_HELP = HELP_MAINMENU_SEND_MESSAGE + HELP_MAINMENU_ACCOUNT_LIST
			+ HELP_MAINMENU_ACCOUNT_SETTINGS + HELP_MAINMENU_HELP;

	public static String ADD_CHANNEL_SUCCESS = "Добавлен канал ";
	public static String ADD_CHANNEL_FAIL = "Ошибка при добавлении канала: ";

	public static String SET_KEY_SUCCESS = "Задан приватный пост ключ для голоса";
	public static String SET_KEY_FAIL = "Ошибка при изменении ключа для голоса: ";

	public static String SET_MIN_COUNT_POSTS_SUCCESS = "Задано минимальное количество сообщений в посте для Голоса";
	public static String SET_MIN_COUNT_POSTS_FAIL = "Ошибка при сохранении: ";

	public static String SET_MAX_COUNT_POSTS_SUCCESS = "Задано максимальное количество сообщений в посте для Голоса";
	public static String SET_MAX_COUNT_POSTS_FAIL = "Ошибка при сохранении: ";

	public static String SET_MESSAGE_ID_SUCCESS = "Задан начальный идентификатор сообщений которые будет отправлены в Голос";
	public static String SET_MESSAGE_ID_FAIL = "Ошибка при добавлении идентификатора: ";

	public static String SET_AUTOSEND_SUCCESS = "Задан признак автоотправки сообщений в Голос";
	public static String SET_AUTOSEND_FAIL = "Ошибка при добавлении призака автоотправки: ";

	public static String GET_STATE_SUCCESS = "Данные о аккаунте: ";
	public static String GET_STATE_FAIL = "Ошибка получения данных о аккаунте: ";

	public static String DELETE_ACCOUNT_SUCCESS = "Удален аккаунт ";
	public static String DELETE_ACCOUNT_FAIL = "Ошибка при удалении аккаунта: ";

	public static String MAINMENU_SUCCESS = "Переход в главное меню ";

	private static String HELP_ACCOUNT_SETTINGS_ADD_CHANNEL = "add channel [название канала] - команда для добавления канала \n";
	private static String HELP_ACCOUNT_SETTINGS_SET_KEY = "set key [приватный ключ] - команда для добавления приватного ключа от Голос \n";
	private static String HELP_ACCOUNT_SETTINGS_SET_MIN_COUNT_POST = "set min count [число] - команда задает максимальное количество сообщений в посте Голос \n";
	private static String HELP_ACCOUNT_SETTINGS_SET_MAX_COUNT_POST = "set max count [число] - команда задает минимальное количество сообщений в посте Голос \n";
	private static String HELP_ACCOUNT_SETTINGS_SET_MESSAGE_ID = "set message id [число] - команда задает начальный идентификатор сообшения \n";
	private static String HELP_ACCOUNT_SETTINGS_SET_AUTOSEND_MESSAGE = "set autosend message [true|false] - команда задает признак автоотправки сообщений в  Голос \n";

	private static String HELP_ACCOUNT_SETTINGS_DELETE_ACCOUNT = "delete account - удалить аккаунт \n";
	private static String HELP_ACCOUNT_SETTINGS_GET_STATE = "get state - информация о аккаунте \n";
	private static String HELP_ACCOUNT_SETTINGS_MAINMENU = "mainmenu - команда для перехода в главное мееню \n";
	private static String HELP_ACCOUNT_SETTINGS_HELP = "help - команда помощь \n";

	public static String ACCOUNT_HELP = HELP_ACCOUNT_SETTINGS_ADD_CHANNEL + HELP_ACCOUNT_SETTINGS_SET_KEY
			+ HELP_ACCOUNT_SETTINGS_SET_MIN_COUNT_POST + HELP_ACCOUNT_SETTINGS_SET_MAX_COUNT_POST
			+ HELP_ACCOUNT_SETTINGS_SET_MESSAGE_ID + HELP_ACCOUNT_SETTINGS_SET_AUTOSEND_MESSAGE
			+ HELP_ACCOUNT_SETTINGS_DELETE_ACCOUNT + HELP_ACCOUNT_SETTINGS_GET_STATE + HELP_ACCOUNT_SETTINGS_MAINMENU
			+ HELP_ACCOUNT_SETTINGS_HELP;

	public static String ERROR_MAX_SIZE_CHANNEL = " превышен лимит подписок на канал.";
	public static String ERROR_MAX_SIZE_ACCOUNT = " превышен лимит аккаунтов.";

}
