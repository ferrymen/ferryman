package ru.maksimov.andrey.ferryman.handlers;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.api.engine.RpcException;

import org.telegram.api.message.TLMessage;
import org.telegram.api.updates.TLUpdateShortMessage;
import org.telegram.bot.structure.IUser;

import ru.maksimov.andrey.ferryman.service.BotService;
import ru.maksimov.andrey.ferryman.service.MenuService;

/**
 * Обработчик сообщений
 * 
 * @author amaksimov
 */
@Component
public class MessageHandler {

	private static final Logger LOG = LogManager.getLogger(MessageHandler.class);

	@Autowired
	private MenuService menuService;

	@Autowired
	private BotService botService;



	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}

	/**
	 * Handler for the request of a contact
	 *
	 * @param user
	 *            User to be answered
	 * @param message
	 *            TLMessage received
	 */
	public void handleMessage(@NotNull IUser user, @NotNull TLMessage message) {
		try {
			handleMessageInternal(user, message.getMessage(), message.getId());
		} catch (RpcException re) {
			LOG.error("Unable send message. " + re.getMessage(), re);
		}
	}

	/**
	 * Handler for the requests of a contact
	 *
	 * @param user
	 *            User to be answered
	 * @param message
	 *            Message received
	 */
	public void handleMessage(@NotNull IUser user, @NotNull TLUpdateShortMessage message) {
		try {
			handleMessageInternal(user, message.getMessage(), message.getId());
		} catch (RpcException re) {
			LOG.error("Unable send message. " + re.getMessage(), re);
		}
	}

	/**
	 * Handle a message from an user
	 * 
	 * @param user
	 *            User that sent the message
	 * @param message
	 *            Message received
	 */
	private void handleMessageInternal(@NotNull IUser user, String message, Integer replayToMsg) throws RpcException {
		if (botService.getCurrentUserId() == user.getUserId()) {
			return;
		}
		message = generateMessage(message, user);
		botService.sendMessageAsReply(user, message, replayToMsg);
		botService.performMarkAsRead(user, 0);
	}

	private String generateMessage(String message, IUser user) {
		return menuService.executeCommand(message, user.getUserId());
		
	}

}
