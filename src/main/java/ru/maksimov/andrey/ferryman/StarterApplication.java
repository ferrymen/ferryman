package ru.maksimov.andrey.ferryman;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Стартер приложения "ferryman"
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
@SpringBootApplication()
@ComponentScan({ "ru.maksimov.andrey.commons", "ru.maksimov.andrey.ferryman",
		"ru.maksimov.andrey.notification.ticket.purchase.client" })
public class StarterApplication {
	static {
		System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
	}

	// https://github.com/rubenlagus/Deepthought
	public static void main(String[] args) {
		// Logger.getGlobal().addHandler(new ConsoleHandler());
		Logger.getGlobal().setLevel(Level.INFO);
		SpringApplication.run(StarterApplication.class, args);
	}
}
