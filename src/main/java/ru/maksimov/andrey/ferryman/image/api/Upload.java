package ru.maksimov.andrey.ferryman.image.api;

import java.io.Serializable;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.message.BasicHeader;

/**
 * Метод загрузки изобрадений для сайта api.imgur.com
 * 
 * @author amaksimov
 */
public class Upload implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String IMAGE_KEY = "image";
	private static final String AUTHORIZATION_KEY = "Authorization";

	private String image;
	private String authorization;

	/**
	 * Получить карту где ключ имя параметра, а значение это сам параметр
	 * 
	 * @return http сущность
	 */
	public HttpEntity getEntity() {
		MultipartEntityBuilder entity = MultipartEntityBuilder.create();
		entity.addTextBody(IMAGE_KEY, image);
		return entity.build();
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public Header getHeader() {
		return new BasicHeader(AUTHORIZATION_KEY, authorization);
	}

}
