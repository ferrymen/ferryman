package ru.maksimov.andrey.ferryman.image.api;

import java.io.Serializable;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Данные из ответа {@link ResponseUpload} 
 * 
 * @author amaksimov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseDataUpload implements Serializable {

	private static final long serialVersionUID = 1L;

	private ResponseError error;
	private String id;
	private String title;
	private String description;
	private Date datetime;
	private String type;
	private boolean animated;
	private int width;
	private int height;
	private long size;
	private int views;
	private int bandwidth;
	private String vote;
	private boolean favorite;
	private String nsfw;
	private String section;
	private String accountUrl;
	private int accountId;
	private boolean isAd;
	private boolean inMostViral;
	private boolean hasSound;
	private int adType;
	private String adUrl;
	private boolean inGallery;
	private String deletehash;
	private String name;
	private String link;

	@JsonProperty("error")
	public ResponseError getError() {
		return error;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("datetime")
	public Date getDatetime() {
		return datetime;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("animated")
	public boolean isAnimated() {
		return animated;
	}

	@JsonProperty("width")
	public int getWidth() {
		return width;
	}

	@JsonProperty("height")
	public int getHeight() {
		return height;
	}

	@JsonProperty("size")
	public long getSize() {
		return size;
	}

	@JsonProperty("views")
	public int getViews() {
		return views;
	}

	@JsonProperty("bandwidth")
	public int getBandwidth() {
		return bandwidth;
	}

	@JsonProperty("vote")
	public String getVote() {
		return vote;
	}

	@JsonProperty("favorite")
	public boolean isFavorite() {
		return favorite;
	}

	@JsonProperty("nsfw")
	public String getNsfw() {
		return nsfw;
	}

	@JsonProperty("section")
	public String getSection() {
		return section;
	}

	@JsonProperty("account_url")
	public String getAccountUrl() {
		return accountUrl;
	}

	@JsonProperty("account_id")
	public int getAccountId() {
		return accountId;
	}

	@JsonProperty("is_ad")
	public boolean isIsAd() {
		return isAd;
	}

	@JsonProperty("in_most_viral")
	public boolean isInMostViral() {
		return inMostViral;
	}

	@JsonProperty("has_sound")
	public boolean isHasSound() {
		return hasSound;
	}

	@JsonProperty("ad_type")
	public int getAdType() {
		return adType;
	}

	@JsonProperty("ad_url")
	public String getAdUrl() {
		return adUrl;
	}

	@JsonProperty("in_gallery")
	public boolean isInGallery() {
		return inGallery;
	}

	@JsonProperty("deletehash")
	public String getDeletehash() {
		return deletehash;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("link")
	public String getLink() {
		return link;
	}

	public void setError(ResponseError error) {
		this.error = error;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setAnimated(boolean animated) {
		this.animated = animated;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public void setBandwidth(int bandwidth) {
		this.bandwidth = bandwidth;
	}

	public void setVote(String vote) {
		this.vote = vote;
	}

	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}

	public void setNsfw(String nsfw) {
		this.nsfw = nsfw;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public void setAccountUrl(String accountUrl) {
		this.accountUrl = accountUrl;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public void setIsAd(boolean isAd) {
		this.isAd = isAd;
	}

	public void setInMostViral(boolean inMostViral) {
		this.inMostViral = inMostViral;
	}

	public void setHasSound(boolean hasSound) {
		this.hasSound = hasSound;
	}

	public void setAdType(int adType) {
		this.adType = adType;
	}

	public void setAd_url(String adUrl) {
		this.adUrl = adUrl;
	}

	public void setIn_gallery(boolean inGallery) {
		this.inGallery = inGallery;
	}

	public void setDeletehash(String deletehash) {
		this.deletehash = deletehash;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
