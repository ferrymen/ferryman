package ru.maksimov.andrey.ferryman.image;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cloudinary.Cloudinary;

import ru.maksimov.andrey.ferryman.util.Utils;
import ru.maksimov.andrey.golos4j.exception.BusinessException;

import org.apache.commons.codec.binary.Base64;

/**
 * Сервис по загрузке картинок на Cloudinary
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class CloudinaryImageService implements ImageService {

	private static final Logger LOG = LogManager.getLogger(CloudinaryImageService.class);
	private static final String CLOUD_NAME = "onixred";
	private static final String API_KEY = "998851641332148";
	private static final String API_SECRET = "SwM2fZIaNKDTt_E7jcckU1bzLN0";
	// [<MIME-type>][;charset=<encoding>][;base64],<data>
	private static final String HEAD_IMAGE = "data:image/png;base64,";
	private static final String RESOURCE_TYPE_OPTION = "resource_type";
	private static final String TAGS_OPTION = "tags";

	private static final String TYPE_JPG = "auto";
	private static final String TAG_FERRYMAN = "ferryman";

	@Override
	public String getImageUrl(List<Byte> bytes) throws BusinessException {
		LOG.info("Start get image urls");
		String url = null;
		byte[]  binaryData = Utils.convert(bytes);
		String image = Base64.encodeBase64String(binaryData);
		int tryNumber = 0;
		int countLoad = 0;
		while (tryNumber < 5 && countLoad < 1) {
			Map<String, String> config = new HashMap<String, String>();
			config.put("cloud_name", CLOUD_NAME);
			config.put("api_key", API_KEY);
			config.put("api_secret", API_SECRET);
			Cloudinary cloudinary = new Cloudinary(config);
			try {
				Map<String, Object> options = new HashMap<String, Object>();
				options.put(RESOURCE_TYPE_OPTION, TYPE_JPG);
				options.put(TAGS_OPTION, TAG_FERRYMAN);

				@SuppressWarnings("unchecked")
				Map<String, Object> uploadResult = cloudinary.uploader().upload(HEAD_IMAGE + image, options);
				if (uploadResult == null) {
					LOG.warn("Unable upload chart: response is null");
				} else if (StringUtils.isNotBlank((String) uploadResult.get("url"))) {
					url = (String) uploadResult.get("url");
					countLoad++;
				} else {
					LOG.warn("Unable upload chart: " + Arrays.toString(uploadResult.keySet().toArray()));
				}
			} catch (IOException se) {
				LOG.warn("Unable upload chart: error execute post " + se.getMessage(), se);
			}

			try {
				Thread.sleep(SLIEEP * tryNumber + 100);
			} catch (InterruptedException ie) {
				throw new BusinessException("Unable upload chart: thread sleep: " + ie.getMessage(), ie);
			}
			tryNumber++;
		}
		LOG.info("End get image urls");
		return url;
	}

	@Override
	public String getImageUrl(String image) throws BusinessException {
		LOG.info("Start get image urls");
		String url = null;
		int tryNumber = 0;
		int countLoad = 0;
		while (tryNumber < 5 && countLoad < 1) {
			Map<String, String> config = new HashMap<String, String>();
			config.put("cloud_name", CLOUD_NAME);
			config.put("api_key", API_KEY);
			config.put("api_secret", API_SECRET);
			Cloudinary cloudinary = new Cloudinary(config);
			try {
				Map<String, Object> options = new HashMap<String, Object>();
				options.put(RESOURCE_TYPE_OPTION, TYPE_JPG);
				options.put(TAGS_OPTION, TAG_FERRYMAN);

				@SuppressWarnings("unchecked")
				Map<String, Object> uploadResult = cloudinary.uploader().upload(HEAD_IMAGE + image, options);
				if (uploadResult == null) {
					LOG.warn("Unable upload chart: response is null");
				} else if (StringUtils.isNotBlank((String) uploadResult.get("url"))) {
					url = (String) uploadResult.get("url");
					countLoad++;
				} else {
					LOG.warn("Unable upload chart: " + Arrays.toString(uploadResult.keySet().toArray()));
				}
			} catch (IOException se) {
				LOG.warn("Unable upload chart: error execute post " + se.getMessage(), se);
			}

			try {
				Thread.sleep(SLIEEP * tryNumber + 100);
			} catch (InterruptedException ie) {
				throw new BusinessException("Unable upload chart: thread sleep: " + ie.getMessage(), ie);
			}
			tryNumber++;
		}
		LOG.info("End get image urls");
		return url;
	}

}
