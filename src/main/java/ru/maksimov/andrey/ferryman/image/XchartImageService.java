package ru.maksimov.andrey.ferryman.image;

import java.util.List;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.maksimov.andrey.ferryman.image.api.HttpUtil;
import ru.maksimov.andrey.ferryman.image.api.ResponseDataUpload;
import ru.maksimov.andrey.ferryman.image.api.ResponseUpload;
import ru.maksimov.andrey.ferryman.image.api.Upload;
import ru.maksimov.andrey.ferryman.util.Utils;
import ru.maksimov.andrey.golos4j.exception.BusinessException;
import ru.maksimov.andrey.golos4j.exception.SystemException;

/**
 * Сервис по загрузке картинок на api.imgur.com
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class XchartImageService implements ImageService {

	private static final Logger LOG = LogManager.getLogger(XchartImageService.class);

	private final String UPLOAD_TO = "https://api.imgur.com/3/upload.json";
	private final String[] CLIENT_IDS = { "e957cac2f7eff88", "c0815ed60570e8e", "dfa77f80712ec14", "fe6adf4f86f7386",
			"6ff0e88968a5165", "e9c7eee10c823ef", "793590a0145ba73", "4c80597e3492d0d", "bf1a961c08fc853",
			"6eeb30e12f3877b" };
	private final String CLIENT_ID_PREFIX = "Client-ID";
	// bot1 4601f8fb022390d44fc8b7bef6d2e8f6913cb5cd
	// bot2 a34a3415c5058263355c9a2843fa3edbfc0fc8b0
	// bot3 774c6c937e64df4aea0e66b92c96cec242d03210
	// bot5 010e7512373fd3de3532258259c9614edce7026a
	// bot6 a194c2cf32c0ec417d16b6158bb2b12e8e514604
	// bot7 08ee895a3ac0fe6256497dfc8a39cde9dc1c5aa7
	// bot8 fe9f55e6e8c1f668a17f0e9559f3e6169ad82b7c
	// bot8 8b9caaa0c89f68e5671158ffa8efc1b58724af35
	// bot9 543bc0f1532ac82ab0f8a94c0c508772a301c675
	private final int SUCCESS = 200;

	@Override
	public String getImageUrl(List<Byte> bytes) throws BusinessException {
		LOG.info("Start get image urls");
		String url = null;
		byte[] binaryData = Utils.convert(bytes);
		String image = Base64.encodeBase64String(binaryData);
		int tryNumber = 0;
		int countLoad = 0;
		while (tryNumber < 5 && countLoad < 1) {
			Upload upload = new Upload();
			upload.setAuthorization(CLIENT_ID_PREFIX + " " + getClientId());
			upload.setImage(image);
			try {
				ResponseUpload responseUpload = HttpUtil.executePost(upload, ResponseUpload.class, UPLOAD_TO);
				if (responseUpload == null) {
					LOG.warn("Unable upload chart: response is null");
				} else if (responseUpload.getStatus() == SUCCESS) {
					ResponseDataUpload date = responseUpload.getDate();
					String link = date.getLink();
					if (StringUtils.isNotBlank(link)) {
						url = link;
						countLoad++;
					}
				} else {
					LOG.warn("Unable upload chart: " + responseUpload.getDate().getError());
				}
			} catch (SystemException se) {
				LOG.warn("Unable upload chart: error execute post " + se.getMessage(), se);
			}catch (Exception e) {
				LOG.warn("Unable upload chart: error execute post " + e.getMessage(), e);
			}

			try {
				Thread.sleep(SLIEEP * tryNumber + 100);
			} catch (InterruptedException ie) {
				throw new BusinessException("Unable upload chart: thread sleep: " + ie.getMessage(), ie);
			}
			tryNumber++;
		}
		LOG.info("End get image urls");
		return url;
	}

	private String getClientId() {
		Random r = new Random();
		int index = r.nextInt(CLIENT_IDS.length - 0) + 0;
		return CLIENT_IDS[index];

	}

	@Override
	public String getImageUrl(String image) throws BusinessException {
		LOG.info("Start get image urls");
		String url = null;
		int tryNumber = 0;
		int countLoad = 0;
		while (tryNumber < 5 && countLoad < 1) {
			Upload upload = new Upload();
			upload.setAuthorization(CLIENT_ID_PREFIX + " " + getClientId());
			upload.setImage(image);
			try {
				ResponseUpload responseUpload = HttpUtil.executePost(upload, ResponseUpload.class, UPLOAD_TO);
				if (responseUpload == null) {
					LOG.warn("Unable upload chart: response is null");
				} else if (responseUpload.getStatus() == SUCCESS) {
					ResponseDataUpload date = responseUpload.getDate();
					String link = date.getLink();
					if (StringUtils.isNotBlank(link)) {
						url = link;
						countLoad++;
					}
				} else {
					LOG.warn("Unable upload chart: " + responseUpload.getDate().getError());
				}
			} catch (SystemException se) {
				LOG.warn("Unable upload chart: error execute post " + se.getMessage(), se);
			}catch (Exception e) {
				LOG.warn("Unable upload chart: error execute post " + e.getMessage(), e);
			}

			try {
				Thread.sleep(SLIEEP * tryNumber + 100);
			} catch (InterruptedException ie) {
				throw new BusinessException("Unable upload chart: thread sleep: " + ie.getMessage(), ie);
			}
			tryNumber++;
		}
		LOG.info("End get image urls");
		return url;
	}

}
