package ru.maksimov.andrey.ferryman.image.api;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Базовая структурва ответа
 * 
 * @author amaksimov
 */
public class ResponseBase implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean success;
	private int status;

	/**
	 * Получить признак успеха выполения запроса
	 */
	@JsonProperty("success")
	public boolean isSuccess() {
		return success;
	}

	/**
	 * Получить код ошибки 200 - успех
	 */
	@JsonProperty("status")
	public int getStatus() {
		return status;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
