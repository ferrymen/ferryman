package ru.maksimov.andrey.ferryman.image.api;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Структура "ошибка в запросе"
 * 
 * @author amaksimov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseError implements Serializable {

	private static final long serialVersionUID = 1L;

	private int code;
	private String message;
	private String type;

	/**
	 * Получить код ошибки
	 */
	@JsonProperty("code")
	public int getCode() {
		return code;
	}

	/**
	 * Получить сообщение ошибки
	 */
	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	/**
	 * Получить тип ошибки
	 */
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
