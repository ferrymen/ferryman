package ru.maksimov.andrey.ferryman.image;

import java.util.List;

import ru.maksimov.andrey.golos4j.exception.BusinessException;

/**
 * Интерфейс сервиса по диаграммами
 * 
 * @author amaksimov
 */
public interface ImageService {

	final static int SLIEEP = 26000;

	String getImageUrl(List<Byte> bytes)
			throws BusinessException;
	String getImageUrl(String  bytes)
			throws BusinessException;

}
