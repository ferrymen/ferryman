package ru.maksimov.andrey.ferryman.image.api;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import ru.maksimov.andrey.golos4j.exception.SystemException;
import ru.maksimov.andrey.golos4j.util.AllowingAllTrustManager;

/**
 * Вспомогательный класс для работы http\https
 * 
 * @author amaksimov
 */
public class HttpUtil {

	private static final Logger LOG = LogManager.getLogger(HttpUtil.class);

	private static int connectTimeout = 30000;
	private static int connectionRequestTimeout = 600000;
	private static int socketTimeout = 300000;

	public static RequestConfig getConfig(int connectTimeout, int connectionRequestTimeout, int socketTimeout) {
		return RequestConfig.custom().setConnectTimeout(connectTimeout)
				.setConnectionRequestTimeout(connectionRequestTimeout).setSocketTimeout(socketTimeout).build();
	}

	public static SSLContext getSSLContext() throws SystemException {
		try {
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(new KeyManager[0], new TrustManager[] { new AllowingAllTrustManager() },
					new SecureRandom());
			SSLContext.setDefault(sslContext);
			return sslContext;
		} catch (NoSuchAlgorithmException nsae) {
			throw new SystemException(" Unable get instance TLS: " + nsae.getMessage() + nsae);
		} catch (KeyManagementException kme) {
			throw new SystemException(" Unable init SSL context: " + kme.getMessage() + kme);
		}
	}

	public static <T> T executePost(Upload method, Class<T> classDto, String url) throws SystemException {
		SSLContext sslContext = HttpUtil.getSSLContext();
		RequestConfig config = HttpUtil.getConfig(connectTimeout, connectionRequestTimeout, socketTimeout);
		CloseableHttpClient httpClient = HttpClientBuilder.create().setSSLContext(sslContext)
				.setDefaultRequestConfig(config).build();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(method.getEntity());
		httpPost.setHeader(method.getHeader());
		try {
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			LOG.info(entity.getContent());
			if (entity != null) {
				LOG.info("Response content length: " + entity.getContentLength());
			}
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = EntityUtils.toString(entity);
			LOG.info("Response content: " + jsonString);
			T getDto;
			if (StringUtils.isBlank(jsonString)) {
				getDto = null;
			} else {
				getDto = mapper.readValue(jsonString, classDto);
			}
			return getDto;
		} catch (ClientProtocolException cpe) {
			throw new SystemException("Unable execute send POST-request: " + cpe.getMessage(), cpe);
		} catch (IOException ioe) {
			throw new SystemException("Unable execute POST-request: " + ioe.getMessage(), ioe);
		}
	}
}

