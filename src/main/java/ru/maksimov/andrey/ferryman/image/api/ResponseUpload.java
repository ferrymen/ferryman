package ru.maksimov.andrey.ferryman.image.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Структура ответа метода @see Upload
 * 
 * @author amaksimov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseUpload extends ResponseBase {

	private static final long serialVersionUID = 1L;

	ResponseDataUpload data;

	@JsonProperty("data")
	public ResponseDataUpload getDate() {
		return data;
	}

	public void setDate(ResponseDataUpload date) {
		this.data = date;
	}

}
