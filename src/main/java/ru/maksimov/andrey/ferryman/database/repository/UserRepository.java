package ru.maksimov.andrey.ferryman.database.repository;


import org.springframework.data.repository.PagingAndSortingRepository;

import ru.maksimov.andrey.ferryman.database.entity.UserGolosEntity;

/**
 * Работа с хранилищем пользователя (данные о голосе)
 * 
 * @author amaksimov
 */
public interface UserRepository extends PagingAndSortingRepository<UserGolosEntity, Long> {

	UserGolosEntity findByUserId(int userId);
}
