package ru.maksimov.andrey.ferryman.database.entity;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.core.type.TypeReference;

import ru.maksimov.andrey.ferryman.structure.Account;
import ru.maksimov.andrey.ferryman.structure.UserMenu;
import ru.maksimov.andrey.ferryman.util.Utils;

/**
 * Пользователь голоса (данные о голосе)
 * 
 * @author amaksimov
 */
@Entity(name = "user_golos" )
public class UserGolosEntity {

	@Id
	@GeneratedValue(generator = "userSequenceGenerator")
	@GenericGenerator(name = "userSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "USER_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1") })
	private Long id;
	@Column(nullable = false)
	private int userId;
	@Column
	private String menu;
	@Column(columnDefinition = "text")
	private String login2Account;

	public Long getId() {
		return id;
	}

	public int getUserId() {
		return userId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getLogin2Account() {
		return login2Account;
	}

	public void setLogin2Accounts(String login2Account) {
		this.login2Account = login2Account;
	}

	public Map<String, Account> getMapLogin2Account() {
		TypeReference<Map<String, Account>> mapType = new TypeReference<Map<String, Account>>() {
		};
		return Utils.<String, Account>convertString2Map(login2Account, mapType);
	}

	public Account getAccountByChannelId(int channelId) {
		Map<String, Account> login2Accaunt = getMapLogin2Account();
		for (String login : login2Accaunt.keySet()) {
			Account accaunt = login2Accaunt.get(login);
			if (accaunt != null) {
				if (channelId == accaunt.getChannelId()) {
					return accaunt;
				}
			}
		}
		return null;
	}

	public void putAccount(Account account) {
		Map<String, Account> login2Account = getMapLogin2Account();
		login2Account.put(account.getLogin(), account);
		this.login2Account = Utils.<String, Account> convertMap2String(login2Account);
	}

	public boolean deleteAccount(String login) {
		Map<String, Account> login2Account = getMapLogin2Account();
		boolean isDelete = false;
		Account account = login2Account.remove(login);
		if (account != null) {
			isDelete = true;
		}
		this.login2Account = Utils.<String, Account> convertMap2String(login2Account);
		return isDelete;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public UserMenu getUserMenu() {
		TypeReference<UserMenu> mapType = new TypeReference<UserMenu>() {
		};
		UserMenu menuDto = Utils.convertString2Object(menu, mapType);
		if (menuDto == null) {
			menuDto = new UserMenu();
		}
		return menuDto;
	}

	public void setUserMenu(UserMenu menuDto) {
		this.menu = Utils.convertObject2String(menuDto);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
