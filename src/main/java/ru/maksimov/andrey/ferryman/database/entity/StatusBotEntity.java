package ru.maksimov.andrey.ferryman.database.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Данные авторизации
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "status_bot")
public class StatusBotEntity {

    public StatusBotEntity() {
    }

    public StatusBotEntity(Integer id) {
        this.id = id;
    }

    @Id
    @Column(nullable = false, unique = true)
    private Integer id;

    private String name;

    private String message;
    private Date updatedAt;

    /**
     * Получить имя бота
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Получить сообщение бота
     * 
     */
    public String getMessage() {
        return message;
    }

    /**
     * Получить дату обновления сообщения
     * 
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Получить идентификатор
     * 
     */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
