package ru.maksimov.andrey.ferryman.database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Данные авторизации
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "authorization_telegram")
public class AuthorizationTelegramEntity {

    public AuthorizationTelegramEntity() {
    }

    public AuthorizationTelegramEntity(String number) {
        this.number = number;
    }

    @Id
    @Column(nullable = false, unique = true)
    private String number;

    @Lob
    private byte[] data;

    public String getNumber() {
        return number;
    }

    public byte[] getData() {
        return data;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
