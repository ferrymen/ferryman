package ru.maksimov.andrey.ferryman.database.repository;

import org.springframework.data.repository.CrudRepository;

import ru.maksimov.andrey.ferryman.database.entity.StatusBotEntity;


/**
 * Работа с хранилищем авторизации
 * 
 * @author amaksimov
 */
public interface StatusBotRepository  extends CrudRepository<StatusBotEntity, Integer> {

}
