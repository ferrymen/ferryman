package ru.maksimov.andrey.ferryman.database;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.telegram.bot.kernel.database.DatabaseManager;
import org.telegram.bot.structure.Chat;
import org.telegram.bot.structure.IUser;

import ru.maksimov.andrey.ferryman.config.Config;
import ru.maksimov.andrey.ferryman.database.entity.StatusBotEntity;
import ru.maksimov.andrey.ferryman.database.entity.UserGolosEntity;
import ru.maksimov.andrey.ferryman.database.repository.StatusBotRepository;
import ru.maksimov.andrey.ferryman.database.repository.UserRepository;
import ru.maksimov.andrey.ferryman.structure.Account;
import ru.maksimov.andrey.ferryman.structure.ChatEntity;
import ru.maksimov.andrey.ferryman.structure.DifferencesDataEntity;
import ru.maksimov.andrey.ferryman.structure.UserEntity;
import ru.maksimov.andrey.ferryman.structure.UserMenu;
import ru.maksimov.andrey.golos4j.exception.BusinessException;

/**
 * Менеджер по работе с БД
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class DatabaseManagerImpl implements DatabaseManager {

	private static DatabaseManagerImpl IMPL;
	private static int MAX_COUNT_UPDATE = 300;

	private Map<Integer, UserEntity> id2User = new HashMap<Integer, UserEntity>();
	{
		id2User.put(0, new UserEntity(0));
	}
	private Map<Integer, ChatEntity> id2Chat = new HashMap<Integer, ChatEntity>();

	private final DifferencesDataEntity differencesData = new DifferencesDataEntity();

	private final Config config;
	private int countUpdate;

	private StatusBotRepository statusBotRepository;
	private UserRepository userRepository;

	private DatabaseManagerImpl() {
		config = Config.getConfig();
		int botId = config.getDifferencesDataBotId();
		int pts = config.getDifferencesDataPts();
		int date = config.getDifferencesDataDate();
		int seq = config.getDifferencesDataSeq();
		updateDifferencesData(botId, pts, date, seq);
	}

	public static DatabaseManagerImpl getInstance() {
		if (IMPL == null) {
			IMPL = new DatabaseManagerImpl();
		}
		return IMPL;
	}

	@Override
	public @Nullable Chat getChatById(int chatId) {
		return id2Chat.get(chatId);
	}

	@Override
	public @Nullable IUser getUserById(int userId) {
		return id2User.get(userId);
	}

	public @Nullable Map<Integer, ChatEntity> getChats() {
		return id2Chat;
	}

	/**
	 * Adds an user to the database
	 *
	 * @param user
	 *            User to be added
	 * @return true if it was added, false otherwise
	 * @see User
	 */
	public boolean addUser(@NotNull UserEntity user) {
		id2User.put(user.getUserId(), user);
		return true;
	}

	public boolean updateUser(@NotNull UserEntity user) {
		UserEntity saveUser = id2User.get(user.getUserId());
		if (saveUser != null) {
			if ((user.getUserHash() == null) || (user.getUserHash() == 0L)) {
				saveUser.setUserHash((long) Types.NUMERIC);
			} else {
				saveUser.setUserHash(user.getUserHash());
			}
			return true;
		}
		return false;
	}

	/**
	 * Adds a chat to the database
	 *
	 * @param chat
	 *            User to be added
	 * @return true if it was added, false otherwise
	 * @see User
	 */
	public boolean addChat(@NotNull ChatEntity chat) {
		id2Chat.put(chat.getId(), chat);
		return true;
	}

	public boolean updateChat(ChatEntity chat) {
		ChatEntity saveChat = id2Chat.get(chat.getId());
		if (saveChat != null) {
			saveChat.setAccessHash(chat.getAccessHash());
			saveChat.setChannel(chat.isChannel());
			return true;
		}
		return false;
	}

	/**
	 * Add a linck Channe-Users to the database
	 *
	 * @param userId
	 *            user id
	 * @param channelId
	 *            channel Id
	 * @throws BusinessException
	 *             error while add the channel
	 */
	public void addChannelForUser(int userId, String accountName, int channelId) throws BusinessException {
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		Account account = login2Account.get(accountName);
		if (account == null) {
			account = new Account();
		}
		account.setLogin(accountName);
		account.setChannelId(channelId);
		setAccount(userId, account);
	}

	/**
	 * Delete a linck Channe-Users to the database
	 *
	 * @param userId
	 *            user id
	 * @param channelId
	 *            channel Id
	 * @throws BusinessException
	 *             error while deleting the channel
	 */
	public void deleteChannelForUser(int userId, String accountName, Integer channelId) throws BusinessException {
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		Account account = login2Account.get(accountName);
		if (account != null) {
			if (channelId.equals(account.getChannelId())) {
				account.setChannelId(null);
				setAccount(userId, account);
			}
		}
	}

	/**
	 * Find all channel name
	 *
	 * @param userId
	 *            user id
	 * @return list channel name
	 * @throws BusinessException
	 *             error while find channels
	 */
	public List<String> findAllChannelNameByUserId(Integer userId) throws BusinessException {
		List<String> channelName = new ArrayList<String>();
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		for (String login : login2Account.keySet()) {
			Account account = login2Account.get(login);
			Integer channelId = account.getChannelId();
			if (channelId != null) {
				ChatEntity chat = id2Chat.get(channelId);
				if (chat != null) {
					channelName.add(chat.getTitle());
				} else {
					channelName.add("Неизвестное название канала channel id:" + channelId);
				}
			}
		}
		return channelName;
	}

	/**
	 * Find all channel name
	 *
	 * @param userId
	 *            user id
	 * @param accountName
	 *            golos login
	 * @return account
	 * @throws BusinessException
	 *             error while find channels
	 */
	public Account findAccountByUserId(Integer userId, String accountName) throws BusinessException {
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		return login2Account.get(accountName);
	}

	@Override
	public @NotNull Map<Integer, int[]> getDifferencesData() {
		final HashMap<Integer, int[]> differencesDatas = new HashMap<>();
		final int[] data = new int[3];
		data[0] = differencesData.getPts();
		data[1] = differencesData.getDate();
		data[2] = differencesData.getSeq();
		differencesDatas.put(differencesData.getBotId(), data);
		return differencesDatas;
	}

	@Override
	public boolean updateDifferencesData(int botId, int pts, int date, int seq) {
		differencesData.setBotId(botId);
		differencesData.setPts(pts);
		differencesData.setDate(date);
		differencesData.setSeq(seq);
		if (countUpdate > MAX_COUNT_UPDATE) {
			config.setDifferencesDataBotId(botId);
			config.setDifferencesDataDate(date);
			config.setDifferencesDataPts(pts);
			config.setDifferencesDataSeq(seq);
			countUpdate = 0;
		}
		countUpdate++;
		return true;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void setStatusBotRepository(StatusBotRepository statusBotRepository) {
		this.statusBotRepository = statusBotRepository;
	}

	

	public Map<String, Account> findAllAccountByUserId(int userId) throws BusinessException {
		UserGolosEntity userGolos = userRepository.findByUserId(userId);
		if (userGolos == null) {
			userGolos = new UserGolosEntity();
			userGolos.setUserId(userId);
		}
		return userGolos.getMapLogin2Account();
	}

	private void setAccount(int userId, Account account) {
		UserGolosEntity userGolos = userRepository.findByUserId(userId);
		if (userGolos == null) {
			userGolos = new UserGolosEntity();
			userGolos.setUserId(userId);
		}
		userGolos.putAccount(account);
		userRepository.save(userGolos);
	}

	/**
	 * Delete account to the database
	 *
	 * @param userId
	 *            user id
	 * @param accountName
	 *            golos login
	 * @throws BusinessException
	 *             error while delete account
	 */
	public void deleteAccountForUser(int userId, String accountName) throws BusinessException {
		UserGolosEntity userGolos = userRepository.findByUserId(userId);
		if (userGolos == null) {
			userGolos = new UserGolosEntity();
			userGolos.setUserId(userId);
		}
		boolean isDelete = userGolos.deleteAccount(accountName);
		if (isDelete) {
			userRepository.save(userGolos);
		}
	}

	public void setUserMenu(int userId, UserMenu userMenu) {
		UserGolosEntity userGolos = userRepository.findByUserId(userId);
		if (userGolos == null) {
			userGolos = new UserGolosEntity();
			userGolos.setUserId(userId);
		}
		userGolos.setUserMenu(userMenu);
		userRepository.save(userGolos);
	}

	/**
	 * Set private key for Account to the database
	 *
	 * @param userId
	 *            user id
	 * @param accountName
	 *            golos login
	 * @throws BusinessException
	 *             error while set key user
	 */
	public void setLoginForUser(int userId, String accountName) throws BusinessException {
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		Account account = login2Account.get(accountName);
		if (account == null) {
			account = new Account();
		}
		account.setLogin(accountName);
		setAccount(userId, account);
	}

	/**
	 * Set private key for Account to the database
	 *
	 * @param userId
	 *            user id
	 * @param accountName
	 *            golos login
	 * @param privateKey
	 *            key user
	 * @throws BusinessException
	 *             error while set key user
	 */
	public void setKeyForUser(int userId, String accountName, String privateKey) throws BusinessException {
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		Account account = login2Account.get(accountName);
		if (account == null) {
			account = new Account();
		}
		account.setLogin(accountName);
		account.setPrivateKey(privateKey);
		setAccount(userId, account);
	}

	/**
	 * Set max size messages for Account to the database
	 *
	 * @param userId
	 *            user id
	 * @param accountName
	 *            golos login
	 * @param maxSizeMessages
	 *            max count messages
	 * @throws BusinessException
	 *             error while set the max size messages
	 */
	public void setMaxSizeMessagesForUser(int userId, String accountName, int maxSizeMessages)
			throws BusinessException {
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		Account account = login2Account.get(accountName);
		if (account == null) {
			account = new Account();
		}
		account.setLogin(accountName);
		account.setMaxCountPosts(maxSizeMessages);
		setAccount(userId, account);
	}

	/**
	 * Set min size messages for Account to the database
	 *
	 * @param userId
	 *            user id
	 * @param accountName
	 *            golos login
	 * @param minSizeMessages
	 *            min count messages
	 * @throws BusinessException
	 *             error while set the min size messages
	 */
	public void setMinSizeMessagesForUser(int userId, String accountName, int minSizeMessages)
			throws BusinessException {
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		Account account = login2Account.get(accountName);
		if (account == null) {
			account = new Account();
		}
		account.setLogin(accountName);
		account.setMinCountPosts(minSizeMessages);
		setAccount(userId, account);
	}

	/**
	 * Set message id for Account to the database
	 *
	 * @param userId
	 *            user id
	 * @param accountName
	 *            golos login
	 * @param messageId
	 *            message id
	 * @throws BusinessException
	 *             error while set the message id
	 */
	public void setMessageIdForUser(int userId, String accountName, int messageId) throws BusinessException {
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		Account account = login2Account.get(accountName);
		if (account == null) {
			account = new Account();
		}
		account.setLogin(accountName);
		account.setMessageId(messageId);
		setAccount(userId, account);
	}

	/**
	 * Set attribute automatic translations message for Account to the database
	 *
	 * @param userId
	 *            user id
	 * @param accountName
	 *            golos login
	 * @param isAutosend
	 *            attribute automatic translations message
	 * @throws BusinessException
	 *             error while set the message id
	 */
	public void setAutosendMessageForUser(int userId, String accountName, boolean isAutosend) throws BusinessException {
		Map<String, Account> login2Account = findAllAccountByUserId(userId);
		Account account = login2Account.get(accountName);
		if (account == null) {
			account = new Account();
		}
		account.setLogin(accountName);
		account.setAutomaticTranslations(isAutosend);
		setAccount(userId, account);
	}

	public long countUsers() {
		return userRepository.count();
	}

	public Set<UserGolosEntity> findAllUsers(Pageable pageable) {
		Set<UserGolosEntity> set = new HashSet<UserGolosEntity>();
		Page<UserGolosEntity> it = userRepository.findAll(pageable);
		if (it != null) {
			for (UserGolosEntity userGolos : it) {
				set.add(userGolos);
			}
		}
		return set;
	}

	public UserGolosEntity findByUserId(int userId) {
		UserGolosEntity userGolos = userRepository.findByUserId(userId);
		return userGolos;
	}

	public void setStatusBot(StatusBotEntity statusBot) {
		StatusBotEntity statusBotEntity = statusBotRepository.findById(statusBot.getId()).orElse(new StatusBotEntity(statusBot.getId()));
		statusBotEntity.setMessage(statusBot.getMessage());
		statusBotEntity.setName(statusBot.getName());
		statusBotEntity.setUpdatedAt(statusBot.getUpdatedAt());
		statusBotRepository.save(statusBotEntity);

	}

	public StatusBotEntity getStatusBot(int userId) {
		return statusBotRepository.findById(userId).orElse(null);
	}
}
