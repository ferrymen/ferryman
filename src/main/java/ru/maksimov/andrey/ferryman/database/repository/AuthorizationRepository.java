package ru.maksimov.andrey.ferryman.database.repository;

import org.springframework.data.repository.CrudRepository;

import ru.maksimov.andrey.ferryman.database.entity.AuthorizationTelegramEntity;

/**
 * Работа с хранилищем авторизации
 * 
 * @author amaksimov
 */
public interface AuthorizationRepository extends CrudRepository<AuthorizationTelegramEntity, String> {
}
